#include "scanner.hh"

namespace mini_java {
void scanner::restart(
    const std::string &name,
    std::istream &input
) {
    loc.initialize(&name);
    yyrestart(&input);
}
} // namespace minijava