#include <iostream>
#include <fstream>
#include <string>

#include <syntax_errors.hh>
#include <driver.hh>
#include <visitor/print_visitor.hh>
#include <visitor/type_table_visitor.hh>
#include <visitor/scope_tree_visitor.hh>
#include <visitor/irtree_build_visitor.hh>
#include <visitor/irtree_print_visitor.hh>
#include <frames/frame_tree_builder.hh>
#include <compilation_errors.hh>

int main(int argc, char* argv[]) {
    if (argc < 1)
        return 1;

    if (argc != 2) {
        std::cout
            << "Usage: "
            << argv[0]
            << " SOURCE"
            << endl;
        return 0;
    }

    const std::string fname(argv[1]);
    mini_java::driver driver;

    std::fstream input(fname);
    auto result = driver.parse(fname, input);
    input.close();

    auto syntax_errors = driver.get_syntax_errors();
    if (syntax_errors.has_error()) {
        syntax_errors.print_errors(std::cout);
        return -1;
    }

    if (!result) {
        std::cout << "AST was not parsed";
        return -1;
    }

    auto ast = *result;

    // PRINT
    std::ofstream fast("ast.out");
    visitor::PrintVisitor::traverse(ast, fast);

    // COMPILE
    mini_java::CompilationErrors errors;

    // TYPE TABLE
    auto table = visitor::TypeTableVisitor::traverse(ast, errors);
    if (errors.has_error()) {
        errors.print_errors(std::cout);
        return 0;
    }

    // SCOPE TREE WALKER
    auto scopes = visitor::ScopeTreeVisitor::traverse(ast, table, errors);
    if (errors.has_error()) {
        errors.print_errors(std::cout);
        return 0;
    }

    // IR TREE
    auto ir_tree = visitor::IRTreeBuildVisitor::traverse(ast, table, scopes);

    std::ofstream fir("irtree.out");
    visitor::IRTreePrintVisitor<PrintFormat::DOT>::traverse(ir_tree, fir);
}
