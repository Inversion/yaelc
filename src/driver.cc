#include "driver.hh"

namespace mini_java {
driver::driver() :
    scanner(),
    parser(scanner, *this) {
    // parser.set_debug_level(1);
}

std::optional<ast::AST> driver::parse(const std::string& name, std::istream& input) {
    scanner.restart(name, input);
    parser();
    return result;
}

mini_java::SyntaxErrors driver::get_syntax_errors() const {
    return syntax_errors;
}
} // namespace minijava
