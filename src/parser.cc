#include <sstream>
#include <vector>

#include "driver.hh"

#include "mini_java_parser.hh"

namespace mini_java {
void parser::error(const mini_java::location& loc, const std::string& msg) {
    driver.add_syntax_error(mini_java::SyntaxError(loc, msg));
}

void parser::report_syntax_error (const context& ctx) const
{
    std::stringstream ss;

    // Report the tokens expected at this point.
    const int amount = ctx.expected_tokens(nullptr, 0);
    vector<symbol_kind_type> expected(amount);
    ctx.expected_tokens(expected.data(), amount);
    for (int i = 0; i < amount; ++i)
        ss  << (i == 0 ? "expected " : " or ")
            << "\"" << symbol_name(expected.at(i)) << "\"";

    // Report the unexpected token.
    symbol_kind_type lookahead = ctx.token();
    if (lookahead != symbol_kind::S_YYEMPTY)
        ss  << (amount == 0 ? "unexpected " : " before ")
            << "\"" << symbol_name(lookahead) << "\"";

    const auto loc = ctx.location();
    const auto msg = ss.str();
    // I dont know why this method is const
    const_cast<parser*>(this)->error(loc, msg);
}
} // namespace mini_java

