#pragma once

#include <iostream>
#include <string>
#include <utility>
#include <optional>

#include <scanner.hh>
#include <ast/ast.hh>
#include <syntax_errors.hh>

// generated
#include "location.hh"
#include "mini_java_parser.hh"

namespace mini_java {
class driver {
public:
    friend class mini_java::parser;

    driver();

    std::optional<ast::AST> parse(const std::string& name, std::istream& input);
    mini_java::SyntaxErrors get_syntax_errors() const;

private:
    mini_java::scanner scanner;
    mini_java::parser parser;

    mini_java::SyntaxErrors syntax_errors;

    std::optional<ast::AST> result;

    void set_result(ast::ClassesRep code) {
        result.emplace(std::move(code));
    }

    void add_syntax_error(mini_java::SyntaxError err) {
        syntax_errors.add(std::move(err));
    }

    void update_syntax_error(mini_java::location loc) {
        syntax_errors.update(loc);
    }
};

} // namespace minijava
