#pragma once

namespace mini_java {

class VoidType;
class IntType;
class FloatType;
class DoubleType;
class CharType;
class StringType;
class ArrayType;
class ClassType;

class TypeVisitor {
public:
    virtual void visit(VoidType& type) = 0;
    virtual void visit(IntType& type) = 0;
    virtual void visit(FloatType& type) = 0;
    virtual void visit(DoubleType& type) = 0;
    virtual void visit(CharType& type) = 0;
    virtual void visit(StringType& type) = 0;
    virtual void visit(ArrayType& type) = 0;
    virtual void visit(ClassType& type) = 0;
};
} // namespace mini_java
