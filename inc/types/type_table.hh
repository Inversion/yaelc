#pragma once

#include <string>
#include <memory>
#include <unordered_map>

#include <types/type.hh>

namespace mini_java {
struct ClassOffsets;
class TypeTable {
    using TypesMap = std::unordered_map<
            std::string,
            std::shared_ptr<Type>>;

    TypesMap types;
public:
    std::shared_ptr<Type> get(const std::string& name) {
        if (types.count(name)) {
            return types[name];
        } else {
            return nullptr;
        }
    }

    friend class TypeTableBuilder;
    friend class ClassOffsets;
};
} // namespace mini_java
