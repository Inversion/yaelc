#pragma once

#include <string>
#include <unordered_map>
#include <list>
#include <memory>

#include <types/type_visitor.hh>
#include <utility>

namespace mini_java {

class Type;
class SimpleType;
class ArrayType;
class ClassType;
class Method;

class Type {
public:
    virtual std::string name() const = 0;
    virtual std::shared_ptr<const Method> method(const std::string& name) const = 0;
    virtual std::shared_ptr<const Type> field(const std::string& name) const = 0;
    virtual void accept(TypeVisitor& visitor) = 0;
    virtual ~Type() = 0;

    static std::string array_for(const std::string& type_name) {
        return type_name + "[]";
    }
};

inline Type::~Type() {}

class SimpleType : public Type {
public:
    virtual ~SimpleType() = 0;
};

inline SimpleType::~SimpleType() {}

class IntType : public SimpleType {
public:
    inline static const std::string NAME = "int";

    void accept(TypeVisitor& visitor) override {
        visitor.visit(*this);
    }

    std::string name() const override {
        return NAME;
    }

    shared_ptr<const Method> method(const string &name) const override {
        return nullptr;
    }

    std::shared_ptr<const Type> field(const std::string &name) const override {
        return nullptr;
    }
};

class FloatType : public SimpleType {
public:
    inline static const std::string NAME = "float";

    void accept(TypeVisitor& visitor) override {
        visitor.visit(*this);
    }

    shared_ptr<const Method> method(const string &name) const override {
        return nullptr;
    }

    std::shared_ptr<const Type> field(const std::string &name) const override {
        return nullptr;
    }

    std::string name() const override {
        return NAME;
    }
};

class DoubleType : public SimpleType {
public:
    inline static const std::string NAME = "double";

    void accept(TypeVisitor& visitor) override {
        visitor.visit(*this);
    }

    shared_ptr<const Method> method(const string &name) const override {
        return nullptr;
    }

    std::shared_ptr<const Type> field(const std::string &name) const override {
        return nullptr;
    }

    std::string name() const override {
        return NAME;
    }
};

class VoidType : public SimpleType {
public:
    inline static const std::string NAME = "void";

    void accept(TypeVisitor& visitor) override {
        visitor.visit(*this);
    }

    shared_ptr<const Method> method(const string &name) const override {
        return nullptr;
    }

    std::shared_ptr<const Type> field(const std::string &name) const override {
        return nullptr;
    }

    std::string name() const override {
        return NAME;
    }
};

class CharType : public SimpleType {
public:
    inline static const std::string NAME = "char";

    void accept(TypeVisitor& visitor) override {
        visitor.visit(*this);
    }

    shared_ptr<const Method> method(const string &name) const override {
        return nullptr;
    }

    std::shared_ptr<const Type> field(const std::string &name) const override {
        return nullptr;
    }

    std::string name() const override {
        return NAME;
    }
};

class StringType : public SimpleType {
public:
    inline static const std::string NAME = "string";

    void accept(TypeVisitor& visitor) override {
        visitor.visit(*this);
    }

    shared_ptr<const Method> method(const string &name) const override {
        return nullptr;
    }

    std::shared_ptr<const Type> field(const std::string &name) const override {
        if (name == "length") {
            //TODO return ptr to length
            return std::make_shared<IntType>();
        }
        return nullptr;
    }

    std::string name() const override {
        return NAME;
    }
};

class ArrayType : public Type {
    std::shared_ptr<const Type> type;

public:
    ArrayType(std::shared_ptr<const Type> type) : type(std::move(type)) {}

    void accept(TypeVisitor& visitor) override {
        visitor.visit(*this);
    }

    shared_ptr<const Method> method(const string &name) const override {
        if (name == "[]") {
            //TODO return ptr to this operator
            return std::make_shared<Method>
                    ("operator []", Modifiers::DEFAULT, Modifiers::DEFAULT, std::make_shared<IntType>());
        }
        return nullptr;
    }

    std::shared_ptr<const Type> field(const std::string &name) const override {
        if (name == "length") {
            return std::make_shared<IntType>();
        }
        return nullptr;
    }

    std::string name() const override {
        return array_for(type->name());
    }
};

class ClassType : public Type {
    using MethodsMap = std::unordered_map<
            std::string,
            std::shared_ptr<Method> >;
    using VariablesMap = std::unordered_map<
            std::string,
            std::shared_ptr<Type>   >;
    using VariablesList = std::list<std::string>;

    const std::string name_;

    MethodsMap methods_;
    VariablesMap vars_types_;
    VariablesList vars_names_;

    std::shared_ptr<ClassType> parent_;

public:
    ClassType(std::string name, std::shared_ptr<ClassType> parent = nullptr) :
        name_(std::move(name)),
        parent_(std::move(parent)) {}

    void accept(TypeVisitor& visitor) override {
        visitor.visit(*this);
    }

    shared_ptr<const Method> method(const string &name) const override {
        if (methods_.count(name)) {
            return methods_.at(name);
        }
        return nullptr;
    }

    shared_ptr<const Type> field(const std::string &name) const override {
        if (vars_types_.count(name)) {
            return vars_types_.at(name);
        }
        return nullptr;
    }

    std::string name() const override {
        return name_;
    }

    const VariablesMap& fields() const {
        return vars_types_;
    }

    friend class TypeTableBuilder;
};

class Method {
    using ArgsMap = std::unordered_map<
            std::string,
            std::shared_ptr<Type>   >;
    using ArgsList = std::list<std::string>;

    Modifiers access_modifier_;
    Modifiers static_modifier_;

    std::shared_ptr<Type> return_type_;

    ArgsMap args_types_;
    ArgsList args_names_;

    const std::string name_;

public:
    Method(
            std::string name,
            Modifiers access_modifier,
            Modifiers static_modifier,
            std::shared_ptr<Type> return_type
        ) :
        access_modifier_(access_modifier),
        static_modifier_(static_modifier),
        return_type_(std::move(return_type)),
        name_(std::move(name)) {}

    const ArgsMap& args() const {
        return args_types_;
    }

    std::shared_ptr<const Type> return_type() const {
        return return_type_;
    }

    const std::string& name() const {
        return name_;
    }

    bool is_static() const {
        return static_modifier_ == Modifiers::STATIC;
    }

    Modifiers get_access_modifier() const {
        return access_modifier_;
    }

    friend class TypeTableBuilder;
};

} // namespace mini_java
