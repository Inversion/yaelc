#pragma once

#include <string>
#include <memory>
#include <unordered_map>
#include <algorithm>
#include <cassert>
#include <utility>

#include <types/type.hh>
#include <types/type_table.hh>
#include <compilation_errors.hh>
#include <with_location.hh>

namespace mini_java {
class TypeTableBuilder {
    TypeTable table_;
    CompilationErrors& errors_;

    std::shared_ptr<ClassType> current_type_ {nullptr};
    std::shared_ptr<Method> current_method_ {nullptr};

    void populate_simple() {
        auto intType = std::make_shared<IntType>();
        auto floatType = std::make_shared<FloatType>();
        auto doubleType = std::make_shared<DoubleType>();
        auto charType = std::make_shared<CharType>();
        auto voidType = std::make_shared<VoidType>();
        auto stringType = std::make_shared<StringType>();

        insert_types(intType);
        insert_types(floatType);
        insert_types(doubleType);
        insert_types(charType);
        insert_types(voidType);
        insert_types(stringType);
    }

    void insert_types(const std::shared_ptr<Type>& type) {
        table_.types[type->name()] = type;
        auto array_type = std::make_shared<ArrayType>(type);
        table_.types[array_type->name()] = array_type;
    }

    void fill_base() {
        assert(current_type_);

        auto base = current_type_->parent_;
        if (!base) {
            return;
        }
        for (const auto&[name, type] : base->fields()) {
            if (!current_type_->vars_types_.count(name)) {
                current_type_->vars_types_[name] = type;
                current_type_->vars_names_.push_back(name);
            }
        }
    }

public:

    TypeTableBuilder(CompilationErrors& errors) : errors_(errors) {
        populate_simple();
    }

    void add_type(WithLoc<const std::string&> name) {
        assert(!current_type_);
        assert(!current_method_);

        if (table_.get(name.val)) {
            errors_.add<TypeRedefinition>(name.val, name.loc);
        }
        else {
            insert_types(std::make_shared<ClassType>(name.val));
        }
    }

    void add_type(WithLoc<const std::string&> name, WithLoc<const std::string&> base) {
        assert(!current_type_);
        assert(!current_method_);

        if (table_.get(name.val)) {
            errors_.add<TypeRedefinition>(name.val, name.loc);
            return;
        }

        auto base_type = table_.get(base.val);
        if (!base_type) {
            errors_.add<UnknownType>(base.val, base.loc);
        }
        auto casted = std::dynamic_pointer_cast<ClassType>(base_type);
        auto new_type = std::make_shared<ClassType>(name.val, casted);
        if (!casted && base_type) {
            errors_.add<NonClassTypeExtension>(name.val, base.val, base.loc);
        }
        insert_types(new_type);
    }

    void in_type(const std::string& name) {
        assert(!current_type_);
        assert(!current_method_);
        assert(table_.get(name));

        auto type = table_.get(name);
        // Bad practice!
        auto casted = std::dynamic_pointer_cast<ClassType>(type);

        assert(casted);

        current_type_ = casted;
    }

    void add_variable(WithLoc<const std::string&> name, WithLoc<const std::string&> type_name) {
        assert(current_type_);
        assert(!current_method_);

        auto type = table_.get(type_name.val);

        if (!type) {
            errors_.add<UnknownType>(type_name.val, type_name.loc);
            return;
        }

        if (current_type_->vars_types_.count(name.val)) {
            errors_.add<VariableRedefinition>(name.val, name.loc);
            return;
        }

        current_type_->vars_types_[name.val] = type;
        current_type_->vars_names_.push_back(name.val);
    }

    void in_add_method(
            WithLoc<const std::string&> name,
            Modifiers access_mod,
            Modifiers static_mod,
            WithLoc<const std::string&> return_type_name
        ) {
        assert(current_type_);
        assert(!current_method_);

        auto return_type = table_.get(return_type_name.val);

        if (!return_type) {
            errors_.add<UnknownType>(return_type_name.val, return_type_name.loc);
//            return;
        }

        if (current_type_->methods_.count(name.val)) {
            errors_.add<MethodRedefinition>(current_type_->name(), name.val, name.loc);
//            return;
        }

        current_method_ = std::make_shared<Method>(
                name.val,
                access_mod,
                static_mod,
                return_type
            );
        current_type_->methods_[name.val] = current_method_;
    }

    void add_argument(WithLoc<const std::string&> name, WithLoc<const std::string&> type_name) {
        assert(current_type_);
        assert(current_method_);

        auto type = table_.get(type_name.val);

        if (!type) {
            errors_.add<UnknownType>(type_name.val, type_name.loc);
            return;
        }

        if (current_method_->args_types_.count(name.val)) {
            errors_.add<MethodArgumentRedefinition>(
                    current_type_->name(),
                    current_method_->name(),
                    name.val,
                    name.loc
                );
            return;
        }

        current_method_->args_types_[name.val] = type;
        current_method_->args_names_.push_back(name.val);
    }

    void out_method() {
        assert(current_type_);
        assert(current_method_);

        current_method_ = nullptr;
    }

    void out_type() {
        assert(current_type_);
        assert(!current_method_);

        fill_base();
        current_type_ = nullptr;
    }

    TypeTable get_table() && {
        auto retval = std::move(table_);
        return retval;
    }
};

}
