#pragma once

#include <list>
#include <memory>
#include <sstream>

#include <types/type.hh>

namespace mini_java {
class CompilationError {
public:
    virtual std::string  msg() = 0;
    virtual mini_java::location loc() = 0;

    virtual ~CompilationError() = default;
};

class UnknownType : public CompilationError {
    public:
        UnknownType(std::string type_name, location location) :
                type_name_(std::move(type_name)),
                location_(location) {}

        std::string msg() override {
            std::stringstream msg;
            msg << "Unknown type " << type_name_ << " here: " << location_;
            return msg.str();
        }

        location loc() override {
            return location_;
        }

    private:
        const std::string type_name_;
        const location location_;
    };

class TypeRedefinition : public CompilationError {
public:
    TypeRedefinition(std::string type_name, location location) :
        type_name_(std::move(type_name)),
        location_(location) {}

    std::string msg() override {
        std::stringstream msg;
        msg << "Redefinition of type " << type_name_ << " here: " << location_;
        return msg.str();
    }

    location loc() override {
        return location_;
    }

private:
    const std::string type_name_;
    const location location_;
};

class UndefinedVariable : public CompilationError {
public:
    UndefinedVariable(std::string var_name, location location) :
    var_name_(std::move(var_name)),
    location_(location) {}

    std::string msg() override {
        std::stringstream msg;
        msg << "Undefined variable " << var_name_ << " here: " << location_;
        return msg.str();
    }

    location loc() override {
        return location_;
    }

private:
    const std::string var_name_;
    const location location_;
};

class VariableRedefinition : public CompilationError {
public:
    VariableRedefinition(std::string var_name, location location) :
            var_name_(std::move(var_name)),
            location_(location) {}

    std::string msg() override {
        std::stringstream msg;
        msg << "Redefinition of variable " << var_name_ << " here: " << location_;
        return msg.str();
    }

    location loc() override {
        return location_;
    }

private:
    const std::string var_name_;
    const location location_;
};

class UndefinedMethod : public CompilationError {
public:
    UndefinedMethod(std::string class_name, std::string method_name, location location) :
            class_name_(std::move(class_name)),
            method_name_(std::move(method_name)),
            location_(location) {}

    std::string msg() override {
        std::stringstream msg;
        msg << "Undefined method " << method_name_
            << " of class " << class_name_ << " here: " << location_;
        return msg.str();
    }

    location loc() override {
        return location_;
    }

private:
    const std::string class_name_;
    const std::string method_name_;
    const location location_;
};

class UndefinedField : public CompilationError {
public:
    UndefinedField(std::string class_name, std::string field_name, std::string variable_name, location location) :
            class_name_(std::move(class_name)),
            field_name_(std::move(field_name)),
            variable_name_(std::move(variable_name)),
            location_(location) {}

    std::string msg() override {
        std::stringstream msg;
        msg << "Undefined field " << field_name_
            << "in variable" << variable_name_ << " of type" << class_name_ << " here: " << location_;
        return msg.str();
    }

    location loc() override {
        return location_;
    }

private:
    const std::string class_name_;
    const std::string field_name_;
    const std::string variable_name_;
    const location location_;
};

class MethodRedefinition : public CompilationError {
public:
    MethodRedefinition(std::string class_name, std::string method_name, location location) :
        class_name_(std::move(class_name)),
        method_name_(std::move(method_name)),
        location_(location) {}

    std::string msg() override {
        std::stringstream msg;
        msg << "Redefinition of method " << method_name_
            << " of class " << class_name_ << " here: " << location_;
        return msg.str();
    }

    location loc() override {
        return location_;
    }

private:
    const std::string class_name_;
    const std::string method_name_;
    const location location_;
};

class MethodArgsCountMismatch : public CompilationError {
public:
    MethodArgsCountMismatch(
            std::string class_name,
            std::string method_name,
            const size_t necessary,
            const size_t supplied,
            location location
        ) :
        class_name_(std::move(class_name)),
        method_name_(std::move(method_name)),
        supplied_(supplied),
        necessary_(necessary),
        location_(location) {}

    std::string msg() override {
        std::stringstream msg;
        msg << "Method arguments size mismatch in " << method_name_
            << " of class " << class_name_
            << ", necessary: " << necessary_
            << ", supplied: " << supplied_ << " here: " << location_;
        return msg.str();
    }

    location loc() override {
        return location_;
    }

private:
    const std::string class_name_;
    const std::string method_name_;
    const size_t necessary_;
    const size_t supplied_;
    const location location_;
};

class MethodArgumentRedefinition : public CompilationError {
public:
    MethodArgumentRedefinition(
            std::string class_name,
            std::string method_name,
            std::string arg_name,
            location location
            ) :
        class_name_(std::move(class_name)),
        method_name_(std::move(method_name)),
        arg_name_(std::move(arg_name)),
        location_(location) {}

    std::string msg() override {
        std::stringstream msg;
        msg << "Method argument redefinition in " << method_name_
            << " of class " << class_name_
            << " of argument " << arg_name_
            << " here: " << location_;
        return msg.str();
    }

    location loc() override {
        return location_;
    }

private:
    const std::string class_name_;
    const std::string method_name_;
    const std::string arg_name_;
    const location location_;
};

class NonClassTypeExtension : public CompilationError {
public:
    NonClassTypeExtension(std::string class_name, std::string parent_name, location location) :
            class_name_(std::move(class_name)),
            parent_name_(std::move(parent_name)),
            location_(location) {}

    std::string msg() override {
        std::stringstream msg;
        msg << "Extension of non class type by " << class_name_
            << " of type " << parent_name_ << " here: " << location_;
        return msg.str();
    }

    location loc() override {
        return location_;
    }

private:
    const std::string class_name_;
    const std::string parent_name_;
    const location location_;
};

class IncompleteTypeExtension : public CompilationError {
public:
    IncompleteTypeExtension(std::string class_name, std::string parent_name, location location) :
            class_name_(std::move(class_name)),
            parent_name_(std::move(parent_name)),
            location_(location) {}

    std::string msg() override {
        std::stringstream msg;
        msg << "Extension class " << class_name_
            << " of incomplete class " << parent_name_ << " here: " << location_;
        return msg.str();
    }

    location loc() override {
        return location_;
    }

private:
    const std::string class_name_;
    const std::string parent_name_;
    const location location_;
};

class CompilationErrors {
public:
    template<typename Error, typename... Args>
    void add(Args&&... args) {
        errors_.emplace_back(new Error(std::move(args)...));
    }

    bool has_error() {
        return !errors_.empty();
    }

    void print_errors(std::ostream& out) const {
        for (const auto& error : errors_) {
            out << error->msg() << std::endl;
        }
    }

    size_t count() const {
        return errors_.size();
    }
private:
    std::list<std::unique_ptr<CompilationError>> errors_;
};
}