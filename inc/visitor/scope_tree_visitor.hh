#pragma once

#include <ast/ast.hh>
#include <scopes/scope_tree_builder.hh>
#include <compilation_errors.hh>

namespace visitor {
    using namespace mini_java;

    class ScopeTreeVisitor : public ast::Visitor {
        ScopeTreeBuilder st_builder;
        TypeTable table_;

        CompilationErrors& errors_;

        std::shared_ptr<const Type> last_type;
        std::shared_ptr<const ClassType> current_class;

        static const int function_depth = 2;
        int depth = 0;

        ScopeTreeVisitor(const TypeTable& table, CompilationErrors& errors) :
                table_(table), errors_(errors) {}

        void begin_scope() {
            depth++;
            if (depth == function_depth + 1) {
                return;
            }
            if (depth == 0) {
                st_builder.begin_class(current_class->name());
            } else {
                st_builder.begin_scope();
            }
        }

        void end_scope() {
            depth--;
            if (depth == function_depth) {
                return;
            }
            if (depth == 0) {
                st_builder.end_class();
            } else {
                st_builder.end_scope();
            }
        }

    public:

        static ScopeTreeWalker traverse(
                const ast::AST& ast,
                const TypeTable& type_table,
                CompilationErrors& errors
        ) {
            ScopeTreeVisitor visitor(type_table, errors);
            visitor.rvisit(ast.code_);
            return std::move(visitor.st_builder).get_tree();
        }

        void visit(const ast::Assign &assign) override {
            rvisit(assign.var);
            rvisit(assign.expr);
        }

        void visit(const ast::Exprs &exprs) override {
            for (const auto& expr : exprs.exprs) {
                rvisit(expr);
            }
        }

        void visit(const ast::ExprEmpty &empty) override {
        }

        void visit(const ast::FieldCall &call) override {
            auto type = st_builder.get_variable_type(call.object);
            if (type) {
                if (!type->field(call.field)) {
                    errors_.add<UndefinedField>(type->name(), call.field, call.object, call.loc);
                }
            } else {
                errors_.add<UndefinedVariable>(call.object, call.loc);
            }
        }

        void visit(const ast::FieldArrayCall &call) override {
            auto type = st_builder.get_variable_type(call.field);
            if (type) {
                //TODO operator []?
                if (!type->method("[]")) {
                    errors_.add<UndefinedMethod>(call.object, "[]", call.loc);
                }
            } else {
                errors_.add<UndefinedVariable>(call.object, call.loc);
            }
            rvisit(call.expr);
        }

        void visit(const ast::Array &arr) override {
            auto type = st_builder.get_variable_type(arr.name);
            if (type) {
                if (!type->method("[]")) {
                    errors_.add<UndefinedMethod>(type->name(), "[]", arr.loc);
                }
            } else {
                errors_.add<UndefinedVariable>(arr.name, arr.loc);
            }
            rvisit(arr.index);
        }

        void visit(const ast::Allocate &allocate) override {
            rvisit(allocate.type);
        }

        void visit(const ast::AllocateArray &allocateArray) override {
            rvisit(allocateArray.quantity);

            auto type_name = Type::array_for(allocateArray.type->name);
            auto type_ = table_.get(type_name);
            if (!type_) {
                errors_.add<UnknownType>(type_name, allocateArray.loc);
            }
        }

        void visit(const ast::Return &return_) override {
            rvisit(return_.expr);
        }

        void visit(const ast::Block &block) override {
            begin_scope();
            for (const auto& stmt : block.stmts) {
                rvisit(stmt);
            }
            end_scope();
        }

        void visit(const ast::IfStmt &stmt) override {
            rvisit(stmt.condition);

            rvisit(stmt.then);

            rvisit(stmt.otherwise);
        }

        void visit(const ast::For &for_) override {
            begin_scope();
            rvisit(for_.init);
            rvisit(for_.condition);
            rvisit(for_.step);
            {
                rvisit(for_.code);
            }
            end_scope();
        }

        void visit(const ast::While &while_) override {
            rvisit(while_.condition);

            rvisit(while_.code);
        }

        void visit(const ast::SimpleType &type) override {
            auto type_ = table_.get(type.name);
            if (!type_) {
                errors_.add<UnknownType>(type.name, type.loc);
            } else {
                last_type = type_;
            }
        }

        void visit(const ast::ArrayType &type) override {
            auto type_ = table_.get(Type::array_for(type.name));
            if (!type_) {
                errors_.add<UnknownType>(type.name, type.loc);
            } else {
                last_type = type_;
            }
        }

        void visit(const ast::Class &class_) override {
            current_class = std::dynamic_pointer_cast<const ClassType>(table_.get(class_.class_name));
            begin_scope();
            if (!current_class) {
                errors_.add<UnknownType>(class_.class_name, class_.loc);
                return;
            }

            auto fields = current_class->fields();
            for (const auto& [name, type] : fields) {
                st_builder.insert_variable(name, type);
            }

            rvisit(class_.body);

            current_class = nullptr;
            end_scope();
        }

        void visit(const ast::Classes &classes) override {
            for (const auto& class_ : classes.decls) {
                rvisit(class_);
            }
        }

        void visit(const ast::ClassDeclMethod &method) override {
            //type_table should handle this method
            auto method_t = current_class->method(method.id);
            assert(method_t);

            st_builder.insert_method(method.id, method_t);
            begin_scope();
            st_builder.insert_variable("this", current_class);
            auto args = method_t->args();
            for (const auto& [name, type] : args) {
                st_builder.insert_variable(name, type);
            }

            rvisit(method.code);

            end_scope();
        }

        void visit(const ast::ClassDeclVar &var) override {
            auto type_rep = var.type;

            auto var_type = table_.get(type_rep->name);
            assert(var_type);

            st_builder.insert_variable(var.name, var_type);
        }

        void visit(const ast::MethodCall &call) override {
            std::string obj_name = call.obj_name;
            auto obj_type = st_builder.get_variable_type(obj_name);
            if (!obj_type) {
                errors_.add<UndefinedVariable>(obj_name, call.loc);
                return;
            }
            auto method_type = obj_type->method(call.method_name);
            if (!method_type) {
                errors_.add<UndefinedMethod>(obj_type->name(), call.method_name, call.loc);
                return;
            }
            auto args_exprs = call.args->exprs;
            if (method_type->args().size() != args_exprs.size()) {
                errors_.add<MethodArgsCountMismatch>(
                        obj_type->name(),
                        call.method_name,
                        method_type->args().size(),
                        args_exprs.size(),
                        call.loc
                );
                return;
            }
            for (const auto& expr : args_exprs) {
                rvisit(expr);
            }
        }

        void visit(const ast::LvalueId &id) override {
            if (!st_builder.check_variable(id.name)) {
                errors_.add<UndefinedVariable>(id.name, id.loc);
            }
        }

        void visit(const ast::LvalueField &field) override {
            rvisit(field.field);
        }

        void visit(const ast::LvalueArray &lvalueArray) override {
            rvisit(lvalueArray.expr);
            auto var_type = st_builder.get_variable_type(lvalueArray.name);
            if (!var_type) {
                errors_.add<UndefinedVariable>(lvalueArray.name, lvalueArray.loc);
            } else {
                if (!var_type->method("[]")) {
                    errors_.add<UndefinedMethod>(var_type->name(), "[]", lvalueArray.loc);
                }
            }
        }

        void visit(const ast::Var &var) override {
            if (!st_builder.check_variable(var.name)) {
                errors_.add<UndefinedVariable>(var.name, var.loc);
            }
        }

        void visit(const ast::Constant &constant) override {
            return;
//            std::shared_ptr<const Type> type;
//            if (std::holds_alternative<int>(constant.val)) {
//                type = table_.get(IntType::NAME);
//            } else if (std::holds_alternative<float>(constant.val)) {
//                type = table_.get(FloatType::NAME);
//            } else if (std::holds_alternative<std::string>(constant.val)) {
//                type = table_.get(StringType::NAME);
//            }
        }

        void visit(const ast::Decls &decls) override {
            for (const auto& decl: decls.decls) {
                rvisit(decl);
            }
        }

        void visit(const ast::StmtDeclVar &var) override {
            rvisit(var.type);
            if (last_type) {
                if (st_builder.check_this_scope_variable(var.name)) {
                    errors_.add<VariableRedefinition>(var.name, var.loc);
                } else {
                    st_builder.insert_variable(var.name, last_type);
                }
            }
            rvisit(var.expr);
        }

        void visit(const ast::SimpleStmt &stmt) override {
            rvisit(stmt.expr);
        }

        template <typename BinOp>
        void visit_binop(BinOp op) {
            rvisit(op.lhs);
            rvisit(op.rhs);
        }

        void visit(const ast::And &anAnd) override {
            visit_binop(anAnd);
        }

        void visit(const ast::Or &anOr) override {
            visit_binop(anOr);
        }

        void visit(const ast::Less &less) override {
            visit_binop(less);
        }

        void visit(const ast::Leq &leq) override {
            visit_binop(leq);
        }

        void visit(const ast::Greater &greater) override {
            visit_binop(greater);
        }

        void visit(const ast::Geq &geq) override {
            visit_binop(geq);
        }

        void visit(const ast::IsEq &eq) override {
            visit_binop(eq);
        }

        void visit(const ast::Mod &mod) override {
            visit_binop(mod);
        }

        void visit(const ast::UNeg &neg) override {
            rvisit(neg.expr);
        }

        void visit(const ast::UMinus &minus) override {
            rvisit(minus.expr);
        }

        void visit(const ast::Add &add) override {
            visit_binop(add);
        }

        void visit(const ast::Diff &diff) override {
            visit_binop(diff);
        }

        void visit(const ast::Product &product) override {
            visit_binop(product);
        }

        void visit(const ast::Slash &slash) override {
            visit_binop(slash);
        }

        void visit(const ast::AccessMod &mod) override {
            std::abort();
        }

        void visit(const ast::Args &args) override {
            //method args handle it method
            std::abort();
        }
    };
}