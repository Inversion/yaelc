#pragma once

#include <irtree/irtree.hpp>
#include <visitor/print_formats.hh>

namespace visitor {
template<PrintFormat format>
class IRTreePrintVisitor;

template<>
class IRTreePrintVisitor<PrintFormat::XML> : public irtree::Visitor {
    Depth level_;
    std::ostream& out_;
public:
    static void traverse(const irtree::IRTree& ir_tree, std::ostream& out) {
        IRTreePrintVisitor visitor(out);

        visitor.traverse(ir_tree);
    }

private:
    IRTreePrintVisitor(std::ostream& out = std::cout) : out_(out) {}

    void traverse(const irtree::IRTree& ir_tree) {
        for (const auto& class_name: ir_tree.get_classes()) {
            for (const auto& method_name: ir_tree.get_methods(class_name)) {
                const auto& label = class_name + "#" + method_name;
                out_ << "<" << label << ">\n";
                const auto& ir = ir_tree.get_ir_for(class_name, method_name);
                rvisit(ir);
                out_ << "<" << label << "/>\n";
            }
        }
    }

    void visit(const irtree::ExprList &exprList) override {
        for (auto& it : exprList.exprs) {
            rvisit(it);
        }
    }

    void visit(const irtree::Temp &temp) override {
        out_ << level_ << "<REG name=\"" << temp.label_.to_string() << "\"/>\n";
    }

    void visit(const irtree::Call &call) override {
        out_    << level_ << "<CALL method=\""
                << call.object << "." << call.method
                << "\">\n";
        ++level_;
        out_ << level_ << "<ARGS>\n";
        ++level_;
        rvisit(call.args);
        --level_;
        out_ << level_ << "</ARGS>\n";
        --level_;
        out_ << level_ << "</CALL>\n";
    }

    void visit(const irtree::Eseq &eseq) override {
        std::abort();
    }

    void visit(const irtree::Mem &mem) override {
        out_ << level_ << "<MEM>\n";
        ++level_;
        rvisit(mem.address);
        --level_;
        out_ << level_ << "</MEM>\n";
    }

    void visit(const irtree::Name &name) override {
        out_    << level_ << "<LABEL name=\""
                << name.label.to_string()
                << "\"/>\n";
    }

    void visit(const irtree::Const &aConst) override {
        out_ << level_ << "<CONST value=\"";
        if (std::holds_alternative<std::string>(aConst.value)) {
            out_ << std::get<std::string>(aConst.value);
        } else if (std::holds_alternative<int>(aConst.value)) {
            out_ << std::get<int>(aConst.value);
        } else if (std::holds_alternative<float>(aConst.value)) {
            out_ << std::get<float>(aConst.value);
        } else if (std::holds_alternative<bool>(aConst.value)) {
            out_ << std::get<bool>(aConst.value);
        }
        out_ << "\"/>\n";
    }

    void print_bin_op(std::string name, irtree::ExprRep lhs, irtree::ExprRep rhs) {
        out_ << level_ << "<" << name << ">\n";
        ++level_;
        out_ << level_ << "<LHS>\n";
        ++level_;
        rvisit(lhs);
        --level_;
        out_ << level_ << "</LHS>\n";
        out_ << level_ << "<RHS>\n";
        ++level_;
        rvisit(rhs);
        --level_;
        out_ << level_ << "</RHS>\n";
        --level_;
    }

    void visit(const irtree::BinOpAdd &add) override {
        print_bin_op("ADD", add.lhs, add.rhs);
    }

    void visit(const irtree::BinOpMul &mul) override {
        print_bin_op("MUL", mul.lhs, mul.rhs);
    }

    void visit(const irtree::BinOpDiv &opDiv) override {
        print_bin_op("DIV", opDiv.lhs, opDiv.rhs);
    }

    void visit(const irtree::BinOpAnd &anAnd) override {
        print_bin_op("AND", anAnd.lhs, anAnd.rhs);
    }

    void visit(const irtree::BinOpOr &anOr) override {
        print_bin_op("OR", anOr.lhs, anOr.rhs);
    }

    void visit(const irtree::BinOpLess &less) override {
        print_bin_op("LESS", less.lhs, less.rhs);
    }

    void visit(const irtree::BinOpLeq &leq) override {
        print_bin_op("LEQ", leq.lhs, leq.rhs);
    }

    void visit(const irtree::BinOpGr &gr) override {
        print_bin_op("GR", gr.lhs, gr.rhs);
    }

    void visit(const irtree::BinOpGeq &geq) override {
        print_bin_op("GEQ", geq.lhs, geq.rhs);
    }

    void visit(const irtree::BinOpIsEq &eq) override {
        print_bin_op("EQ", eq.lhs, eq.rhs);
    }

    void visit(const irtree::BinOpSub &sub) override {
        print_bin_op("SUB", sub.lhs, sub.rhs);
    }

    void visit(const irtree::BinOpMod &mod) override {
        print_bin_op("MOD", mod.lhs, mod.rhs);
    }

    void visit(const irtree::UnOpNeg &neg) override {
        out_ << level_ << "<NEG>\n";
        ++level_;
        rvisit(neg.lhs);
        --level_;
        out_ << level_ << "</NEG>\n";
    }

    void visit(const irtree::UnOpSub &sub) override {
        out_ << level_ << "<SUB>\n";
        ++level_;
        rvisit(sub.lhs);
        --level_;
        out_ << level_ << "</SUB>\n";
    }

    void visit(const irtree::Move &move) override {
        out_ << level_ << "<MOVE>\n";
        ++level_;
        out_ << level_ << "<FROM>\n";
        ++level_;
        rvisit(move.source);
        --level_;
        out_ << level_ << "</FROM>\n";
        out_ << level_ << "<TO>\n";
        ++level_;
        rvisit(move.dest);
        --level_;
        out_ << level_ << "</TO>\n";
        --level_;
        out_ << level_ << "</MOVE>\n";
    }

    void visit(const irtree::Label &label) override {
        out_    << level_ << "<LABEL name=\""
                << label.label_name.to_string()
                << "\"/>\n";
    }

    void visit(const irtree::Jump &jump) override {
        out_ << level_ << "<JUMP>\n";
        ++level_;
        rvisit(jump.jump_to);
        --level_;
        out_ << level_ << "</JUMP>\n";
    }

    void visit(const irtree::CJump &jump) override {
        out_ << level_ << "<CJUMP>\n";
        ++level_;
        out_ << level_ << "<COND>\n";
        ++level_;
        rvisit(jump.bin_op);
        --level_;
        out_ << level_ << "</COND>\n";
        out_ << level_ << "<TRUE>\n";
        ++level_;
        rvisit(jump.jump_true);
        --level_;
        out_ << level_ << "</TRUE>\n";
        out_ << level_ << "<FALSE>\n";
        ++level_;
        rvisit(jump.jump_false);
        --level_;
        out_ << level_ << "</FALSE>\n";
        --level_;
        out_ << level_ << "</CJUMP>\n";
    }

    void visit(const irtree::Seq &seq) override {
        out_ << level_ << "<SEQ>\n";
        ++level_;
        for (auto& stmt : seq.stmts) {
            rvisit(stmt);
        }
        --level_;
        out_ << level_ << "</SEQ>\n";
    }

    void visit(const irtree::StmtExpr &expr) override {
        out_ << level_ << "<EXPR>\n";
        ++level_;
        rvisit(expr.expr);
        --level_;
        out_ << level_ << "</EXPR>\n";
    }
};

template<>
class IRTreePrintVisitor<PrintFormat::DOT> : public irtree::Visitor {
    std::ostream& out_;
    int meta_node_counter_;
public:
    static void traverse(const irtree::IRTree& ir_tree, std::ostream& out) {
        IRTreePrintVisitor visitor(out);
        visitor.traverse(ir_tree);
    }

private:
    template<typename T>
    inline void node_name(const T& id) {
        out_ << "node_at" << &id;
    }

    template<typename T>
    inline void node(const T& id, const std::string& label) {
        node_name(id);
        out_ << " [shape=box, label=\"" << label << "\"];\n";
    }

    template<typename T>
    inline void meta_node(const T& from, const std::string& node, const std::string& label) {
        const auto& meta_name = "meta_node_" + std::to_string(meta_node_counter_++);
        out_ <<  meta_name << " [shape=box, label=\"" << node << "\"];\n";
        node_name(from);
        out_ << " -> " << meta_name;
        out_ << " [label=\"" << label << "\"];\n";
    }

    template<typename T, typename U>
    inline void edge(const T& from, const util::Rep<U>& to, const std::string& label="") {
        node_name(from);
        out_ << " -> ";
        node_name(*to);
        out_ << " [label=\"" << label << "\"];\n";
    }

    IRTreePrintVisitor(std::ostream& out) :
        out_(out),
        meta_node_counter_(0) {}

    void traverse(const irtree::IRTree& ir_tree) {
        out_ << "digraph Program {\n";
        for (const auto& class_name: ir_tree.get_classes()) {
            for (const auto& method_name: ir_tree.get_methods(class_name)) {
                const auto& label = class_name + "#" + method_name;
                out_ << "subgraph {\n";
                const auto& ir = ir_tree.get_ir_for(class_name, method_name);
                rvisit(ir);
                out_ << "label=\"" << label << "\";\n";
                out_ << "}\n";
            }
        }
        out_ << "}\n";
    }

    void visit(const irtree::ExprList &exprList) override {
        node(exprList, "EXPRS");
        int counter = 0;
        for (auto& it : exprList.exprs) {
            edge(exprList, it, to_string(counter++));
            rvisit(it);
        }
    }

    void visit(const irtree::Temp &temp) override {
        node(temp, "REG " + temp.label_.to_string());
    }

    void visit(const irtree::Call &call) override {
        node(call, "CALL " + call.object + "." + call.method);
        edge(call, call.args, "ARGS");
        rvisit(call.args);
    }

    void visit(const irtree::Eseq &eseq) override {
        std::abort();
    }

    void visit(const irtree::Mem &mem) override {
        node(mem, "MEM");
        edge(mem, mem.address, "ADDR");
        rvisit(mem.address);
    }

    void visit(const irtree::Name &name) override {
        node(name, "NAME " + name.label.to_string());
    }

    void visit(const irtree::Const &aConst) override {
        std::string label = "CONST ";
        if (std::holds_alternative<std::string>(aConst.value)) {
            label += std::get<std::string>(aConst.value);
        } else if (std::holds_alternative<int>(aConst.value)) {
            label += std::to_string(std::get<int>(aConst.value));
        } else if (std::holds_alternative<float>(aConst.value)) {
            label += std::to_string(std::get<float>(aConst.value));
        } else if (std::holds_alternative<bool>(aConst.value)) {
            label += std::to_string(std::get<bool>(aConst.value));
        }
        node(aConst, label);
    }

    template<int id>
    inline void print_bin_op(
            const irtree::BinOp<id>& binop,
            std::string name,
            irtree::ExprRep lhs,
            irtree::ExprRep rhs
        ) {
        node(binop, name);
        edge(binop, lhs, "LHS");
        rvisit(lhs);
        edge(binop, rhs, "RHS");
        rvisit(rhs);
    }

    void visit(const irtree::BinOpAdd &add) override {
        print_bin_op(add, "ADD", add.lhs, add.rhs);
    }

    void visit(const irtree::BinOpMul &mul) override {
        print_bin_op(mul, "MUL", mul.lhs, mul.rhs);
    }

    void visit(const irtree::BinOpDiv &opDiv) override {
        print_bin_op(opDiv, "DIV", opDiv.lhs, opDiv.rhs);
    }

    void visit(const irtree::BinOpAnd &anAnd) override {
        print_bin_op(anAnd, "AND", anAnd.lhs, anAnd.rhs);
    }

    void visit(const irtree::BinOpOr &anOr) override {
        print_bin_op(anOr, "OR", anOr.lhs, anOr.rhs);
    }

    void visit(const irtree::BinOpLess &less) override {
        print_bin_op(less, "LESS", less.lhs, less.rhs);
    }

    void visit(const irtree::BinOpLeq &leq) override {
        print_bin_op(leq, "LEQ", leq.lhs, leq.rhs);
    }

    void visit(const irtree::BinOpGr &gr) override {
        print_bin_op(gr, "GR", gr.lhs, gr.rhs);
    }

    void visit(const irtree::BinOpGeq &geq) override {
        print_bin_op(geq, "GEQ", geq.lhs, geq.rhs);
    }

    void visit(const irtree::BinOpIsEq &eq) override {
        print_bin_op(eq, "EQ", eq.lhs, eq.rhs);
    }

    void visit(const irtree::BinOpSub &sub) override {
        print_bin_op(sub, "SUB", sub.lhs, sub.rhs);
    }

    void visit(const irtree::BinOpMod &mod) override {
        print_bin_op(mod, "MOD", mod.lhs, mod.rhs);
    }

    void visit(const irtree::UnOpNeg &neg) override {
        node(neg, "NEG");
        edge(neg, neg.lhs, "ARG");
        rvisit(neg.lhs);
    }

    void visit(const irtree::UnOpSub &sub) override {
        node(sub, "SUB");
        edge(sub, sub.lhs, "ARG");
        rvisit(sub.lhs);
    }

    void visit(const irtree::Move &move) override {
        node(move, "MOVE");
        edge(move, move.source, "SOURCE");
        rvisit(move.source);
        edge(move, move.dest, "DEST");
        rvisit(move.dest);
    }

    void visit(const irtree::Label &label) override {
        node(label, "LABEL " + label.label_name.to_string());
    }

    void visit(const irtree::Jump &jump) override {
        node(jump, "JUMP");
        const auto& node = jump.jump_to->label_name.to_string();
        meta_node(jump, node, "LABEL");
    }

    void visit(const irtree::CJump &jump) override {
        node(jump, "CJUMP");
        edge(jump, jump.bin_op, "COND");
        rvisit(jump.bin_op);
        const auto& true_node = jump.jump_true->label_name.to_string();
        meta_node(jump, true_node, "TRUE");
        const auto& false_node = jump.jump_false->label_name.to_string();
        meta_node(jump, false_node, "FALSE");
    }

    void visit(const irtree::Seq &seq) override {
        node(seq, "SEQ");
        int counter = 0;
        for (auto& stmt : seq.stmts) {
            edge(seq, stmt, to_string(counter++));
            rvisit(stmt);
        }
    }

    void visit(const irtree::StmtExpr &expr) override {
        node(expr, "EXPR");
        edge(expr, expr.expr);
        rvisit(expr.expr);
    }
};
}