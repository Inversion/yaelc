#pragma once

namespace visitor {
class Depth {
public:
    int level = 0;
    Depth(int level = 0) : level(level) {}

    auto operator++ () -> int {
        return level++;
    }
    auto operator-- () -> int {
        return level--;
    }
    friend auto operator<<(std::ostream& os, const Depth& dt) -> std::ostream&;
};

inline std::ostream& operator<<(std::ostream& os, const Depth& depth) {
    std::string level(depth.level, '\t');
    os << level;
    return os;
}
} // namespace visitor

