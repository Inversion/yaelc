#pragma once

#include <ast/ast.hh>
#include <types/type_table_builder.hh>
#include <compilation_errors.hh>

namespace visitor {
using namespace mini_java;

class TypeTableVisitor : public ast::Visitor {
    TypeTableBuilder ttb;

    std::string visited_type_name;

    enum {
        TYPE_NAMES,
        TYPE_MEMBERS
    } state {};

    TypeTableVisitor(CompilationErrors& errors) : ttb(errors) {}

public:

    static TypeTable traverse(const ast::AST& ast, CompilationErrors& errors) {
        TypeTableVisitor visitor(errors);

        visitor.state = TYPE_NAMES;
        visitor.rvisit(ast.code_);
        visitor.state = TYPE_MEMBERS;
        visitor.rvisit(ast.code_);

        return std::move(visitor.ttb).get_table();
    }

    void visit(const ast::Args &args) override {
        for (auto arg : args.args) {
            rvisit(arg.type);
            ttb.add_argument(
                    {arg.name, args.loc}, // TODO individual arg loc
                    {visited_type_name, arg.type->loc}
                );
        }
    }

    void visit(const ast::SimpleType &type) override {
        visited_type_name = type.name;
    }

    void visit(const ast::ArrayType &type) override {
        visited_type_name = Type::array_for(type.name);
    }

    void visit(const ast::Class &aClass) override {
        switch (state) {
            case TYPE_NAMES:
                if (aClass.base_class.empty()) {
                    ttb.add_type({aClass.class_name, aClass.loc});
                }
                else {
                    ttb.add_type(
                            {aClass.class_name, aClass.loc},
                            {aClass.base_class, aClass.loc}
                    );
                }
                break;
            case TYPE_MEMBERS:
                ttb.in_type(aClass.class_name);
                rvisit(aClass.body);
                ttb.out_type();
                break;
            default:
                std::abort();
        }
    }

    void visit(const ast::Classes &classes) override {
        for (auto a_class: classes.decls)
            rvisit(a_class);
    }

    void visit(const ast::ClassDeclMethod &method) override {
        rvisit(method.type);
        ttb.in_add_method(
                {method.id, method.loc}, // TODO individual loc
                method.modifier->modifier,
                method.is_static,
                {visited_type_name, method.type->loc}
            );
        rvisit(method.args);
        ttb.out_method();
    }

    void visit(const ast::ClassDeclVar &var) override {
        rvisit(var.type);
        ttb.add_variable(
                {var.name, var.loc},
                {visited_type_name, var.type->loc}
            );
    }

    void visit(const ast::Decls &decls) override {
        for (auto decl : decls.decls) {
            rvisit(decl);
        }
    }

    void visit(const ast::Assign &assign) override {
        std::abort();
    }

    void visit(const ast::Exprs &exprs) override {
        std::abort();
    }

    void visit(const ast::ExprEmpty &empty) override {
        std::abort();
    }

    void visit(const ast::FieldCall &call) override {
        std::abort();
    }

    void visit(const ast::Array &array) override {
        std::abort();
    }

    void visit(const ast::Allocate &allocate) override {
        std::abort();
    }

    void visit(const ast::AllocateArray &array) override {
        std::abort();
    }

    void visit(const ast::Return &aReturn) override {
        std::abort();
    }

    void visit(const ast::AccessMod &mod) override {
        std::abort();
    }

    void visit(const ast::Block &block) override {
        std::abort();
    }

    void visit(const ast::IfStmt &stmt) override {
        std::abort();
    }

    void visit(const ast::For &aFor) override {
        std::abort();
    }

    void visit(const ast::While &aWhile) override {
        std::abort();
    }

    void visit(const ast::MethodCall &call) override {
        std::abort();
    }

    void visit(const ast::Var &var) override {
        std::abort();
    }

    void visit(const ast::Constant &constant) override {
        std::abort();
    }

    void visit(const ast::SimpleStmt &stmt) override {
        std::abort();
    }

    void visit(const ast::And &anAnd) override {
        std::abort();
    }

    void visit(const ast::Or &anOr) override {
        std::abort();
    }

    void visit(const ast::Less &less) override {
        std::abort();
    }

    void visit(const ast::Leq &leq) override {
        std::abort();
    }

    void visit(const ast::Greater &greater) override {
        std::abort();
    }

    void visit(const ast::Geq &geq) override {
        std::abort();
    }

    void visit(const ast::IsEq &eq) override {
        std::abort();
    }

    void visit(const ast::Mod &mod) override {
        std::abort();
    }

    void visit(const ast::UNeg &neg) override {
        std::abort();
    }

    void visit(const ast::UMinus &minus) override {
        std::abort();
    }

    void visit(const ast::Add &add) override {
        std::abort();
    }

    void visit(const ast::Diff &diff) override {
        std::abort();
    }

    void visit(const ast::Product &product) override {
        std::abort();
    }

    void visit(const ast::Slash &slash) override {
        std::abort();
    }

    void visit(const ast::FieldArrayCall &call) override {
        std::abort();
    }

    void visit(const ast::LvalueId &id) override {
        std::abort();
    }

    void visit(const ast::LvalueField &field) override {
        std::abort();
    }

    void visit(const ast::LvalueArray &lvalueArray) override {
        std::abort();
    }

    void visit(const ast::StmtDeclVar &var) override {
        std::abort();
    }
};
} // namespace visitor
