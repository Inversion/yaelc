#pragma once

#include <ast/ast.hh>
#include <visitor/depth.hh>

namespace visitor {
class PrintVisitor : public ast::Visitor {
    Depth level;
    std::ostream& out_;

    PrintVisitor(std::ostream& out = std::cout) : out_(out), level() {}

public:
    static void traverse(const ast::AST& ast, std::ostream& out) {
        PrintVisitor visitor(out);

        visitor.traverse(ast);
    }

    void visit(const ast::LvalueId &id) override {
      out_ << level << id.name << std::endl;
    }

    void visit(const ast::LvalueField &field) override {
      rvisit(field.field);
    }

    void visit(const ast::LvalueArray &lvalueArray) override {
      out_ << level << lvalueArray.name << "[" << std::endl;
      ++level;
      rvisit(lvalueArray.expr);
      --level;
      out_ << level << "]" << std::endl;
    }

    void traverse(const ast::AST& ast) {
        rvisit(ast.code_);
    }

    void visit(const ast::Exprs& exprs) override {
        out_ << level
                  << "<exprs>\n";
        ++level;
        for (const ast::ExprRep& expr: exprs.exprs)
            rvisit(expr);
        --level;

        out_ << level
                  << "</exprs>\n";
    }

    void visit(const ast::ExprEmpty& e) override {
        out_ << level << "<empty expr>\n";
    }

    void visit(const ast::FieldCall& field) override {
        out_ << level << "<invocation "
                << field.object << "." << field.field
                << ">" << std::endl;
    }

    void visit(const ast::StmtDeclVar& declVar) override {
        rvisit(declVar.type);
        out_ << level << "<decl Var " << declVar.name << ">\n";
        ++level;
        out_ << level << "<value>" << std::endl;
        ++level;
        rvisit(declVar.expr);
        --level;
        out_ << level << "</value>" << std::endl;
        --level;
        out_ << level << "</decl Var>\n";
    }

    void visit(const ast::SimpleStmt& stmt) override {
        rvisit(stmt.expr);
    }

    void visit(const ast::Array& array) override {
        out_ << level
                  << "this->" << array.name << "[\n";
        ++level;
        rvisit(array.index);
        --level;
        out_ << level << "]\n";
    }

    void visit(const ast::Allocate& allocate) override {
        out_ << level << "<allocate type>\n";
        ++level;
        rvisit(allocate.type);
        --level;
        out_ << level << "</allocate type>\n";
    }

    void visit(const ast::AllocateArray& allocate) override {
        out_ << level << "<allocate array of type>\n";
        ++level;
        rvisit(allocate.type);
        --level;
        out_ << level << "</allocate array of type>\n";
        out_ << level << "<quantity>" << std::endl;
        ++level;
        rvisit(allocate.quantity);
        --level;
        out_ << level << "</quantity>" << std::endl;
    }

    void visit(const ast::Return& ret) override {
        out_ << level << "<return>\n";
        ++level;
        rvisit(ret.expr);
        --level;
        out_ << level << "</return>\n";
    }

    void visit(const ast::AccessMod& accessMod) override {
        out_ << level << "<modifier>\n";
    }

    void visit(const ast::Args& args) override {
        out_ << level << "<args>\n";
        ++level;
        for (const ast::Args::Arg& arg: args.args) {
            rvisit(arg.type);
            out_ << level << "<name " << arg.name << ">" << std::endl;
        }
        --level;
        out_ << level << "</args>\n";
    }

    void visit(const ast::Block& block) override {
        out_ << level
                  << "<block>\n";
        ++level;
        for (const ast::StmtRep& stmt: block.stmts)
            rvisit(stmt);
        --level;

        out_ << level
                  << "</block>\n";
    }

    void visit(const ast::IfStmt& ifstmt) override {

        out_ << level
                  << "<if>\n";
        ++level;
        rvisit(ifstmt.condition);
        rvisit(ifstmt.then);
        rvisit(ifstmt.otherwise);
        --level;

        out_ << level
                  << "</if>\n";
    }

    void visit(const ast::For& loop) override {
        out_ << level
                  << "<loop>\n";

        ++level;
        rvisit(loop.init);
        rvisit(loop.step);
        rvisit(loop.condition);
        rvisit(loop.code);
        --level;

        out_ << level
                  << "</loop>\n";
    }

    void visit(const ast::Assign& assign) override {
        out_ << level
                  << "<assign var>\n";
        ++level;
        rvisit(assign.var);
        rvisit(assign.expr);
        --level;

        out_ << level
                  << "</assign>\n";
    }

    void visit(const ast::While& while_) override {
        out_ << level << "<while>\n";
        ++level;
        out_ << level << "<condition>\n";
        ++level;
        rvisit(while_.condition);
        --level;
        out_ << level << "</condition>\n";
        out_ << level << "<code>\n";
        ++level;
        rvisit(while_.code);
        --level;
        out_ << level << "</code>\n";
        --level;
        out_ << level << "</while>\n";
    }

    void visit(const ast::SimpleType& type) override {
        out_ << level << "<simple_type>\n";
        ++level;
        out_ << level << type.name << std::endl;
        --level;
        out_ << level << "</simple_type>\n";
    }

    void visit(const ast::ArrayType& type) override {
        out_ << level << "<array_type>\n";
        ++level;
        out_ << level << type.name << std::endl;
        --level;
        out_ << level << "</array_type>\n";
    }

    void visit(const ast::Class& class_) override {
        out_ << level << "<class " << class_.class_name;
        if (!class_.base_class.empty())
            out_ << " extends " << class_.base_class;
        out_ << ">\n";
        ++level;
        rvisit(class_.body);
        --level;
        out_ << level << "</class>\n";
    }

    void visit(const ast::Classes& classes) override {
        out_ << level << "<classes>\n";
        ++level;
        for (const ast::ClassRep &class_ : classes.decls) {
            rvisit(class_);
        }
        --level;
        out_ << level << "</classes>\n";
    }


    void visit(const ast::Decls& decl) override {
        out_ << level << "<decls>\n";
        ++level;
        for (const auto &decl_ : decl.decls) {
            rvisit(decl_);
        }
        --level;
        out_ << level << "</decls>\n";
    }
    
    void visit(const ast::ClassDeclVar& declVar) override {
        rvisit(declVar.type);
        out_ << level << "<decl Var " << declVar.name << ">\n";
        out_ << level << "<value>" << std::endl;
        ++level;
        rvisit(declVar.expr);
        --level;
        out_ << level << "</decl Var>\n";
    }

    void visit(const ast::ClassDeclMethod& declMethod) override {
        out_ << level << "<decl Method " << declMethod.id << ">\n";
        ++level;
        out_ << level << "<return>" << std::endl;
        ++level;
        rvisit(declMethod.type);
        --level;
        out_ << level << "</return>" << std::endl;

        rvisit(declMethod.args);

        out_ << level << "<access mod>" << std::endl;
        ++level;
        rvisit(declMethod.modifier);
        --level;
        out_ << level << "</access mod>" << std::endl;
        out_ << level << "<static mod: " << (declMethod.is_static == ast::Modifiers::STATIC) << ">" <<  std::endl;
        out_ << level << "<code>" << std::endl;
        ++level;
        rvisit(declMethod.code);
        --level;
        out_ << level << "</code>" << std::endl;
        --level;
        out_ << level << "</decl Method>\n";
    }

    void visit(const ast::MethodCall& methodCall) override {
        out_ << level << "<call " << methodCall.obj_name << "." << methodCall.method_name << ">\n";
        ++level;
        out_ << level << "<args>\n";
        ++level;
        rvisit(methodCall.args);
        --level;
        out_ << level << "</args>\n";
        --level;
        out_ << level << "</call>\n";
    }

    void visit(const ast::Var& var) override {
        out_ << level << "<variable " << var.name << ">\n";
    }

    template <int T>
    void PrintBinaryOp(const ast::BinOp<T> op, const std::string& op_name) {
        out_ << level << "<" << op_name << ">\n";
        ++level;
        rvisit(op.lhs);
        rvisit(op.rhs);
        --level;

        out_ << level << "</" << op_name << ">\n";
    }

    void visit(const ast::And& op) override {
        PrintBinaryOp(op, "And");
    }

    void visit(const ast::Or& op) override {
        PrintBinaryOp(op, "Or");
    }

    void visit(const ast::Less& op) override {
        PrintBinaryOp(op, "Less");
    }

    void visit(const ast::Leq& op) override {
        PrintBinaryOp(op, "Leq");
    }

    void visit(const ast::Greater& op) override {
        PrintBinaryOp(op, "Greater");
    }

    void visit(const ast::Geq& op) override {
        PrintBinaryOp(op, "Geq");
    }

    void visit(const ast::IsEq& op) override {
        PrintBinaryOp(op, "IsEq");
    }

    void visit(const ast::Mod& op) override {
        PrintBinaryOp(op, "Mod");
    }


    void visit(const ast::Add& op) override {
        PrintBinaryOp(op, "Add");
    }

    void visit(const ast::Diff& op) override {
        PrintBinaryOp(op, "Diff");
    }

    void visit(const ast::Product& op) override {
        PrintBinaryOp(op, "Product");
    }

    void visit(const ast::Slash& op) override {
        PrintBinaryOp(op, "Slash");
    }

    void visit(const ast::Constant& cnst) override {
        out_ << level
                  << "<" << "constant"
                  << ">\n";
    }

    void visit(const ast::UNeg& op) override {
        out_ << level
                  << "<Neg>\n";
        ++level;
        rvisit(op.expr);
        --level;

        out_ << level
                  << "</Neg>\n";
    }

    void visit(const ast::UMinus& op) override {
        out_ << level
                  << "<un_minus>\n";
        ++level;
        rvisit(op.expr);
        --level;

        out_ << level
                  << "</un_minus>\n";
    }

    void visit(const ast::FieldArrayCall &call) override {
        out_ << level << "<call array field " << call.object << "." << call.field << ">\n";
        ++level;
        rvisit(call.expr);
        --level;
        out_ << level << "</call array field>\n";
    }
};
}