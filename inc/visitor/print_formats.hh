#pragma once

enum class PrintFormat {
    XML,
    DOT
};
