#include <irtree/irtree.hpp>
#include <ast/ast.hh>
#include <visitor/scope_tree_visitor.hh>
#include <visitor/type_table_visitor.hh>
#include <scopes/scope_tree.hh>
#include <frames/frame_tree_builder.hh>
#include <frames/frame_tree_walker.hh>
#include <frames/class_offsets.hh>
#include <utility>

namespace visitor {
class IRTreeBuildVisitor : ast::Visitor {
    using ReturnType = std::variant<irtree::ExprRep, irtree::StmtRep, irtree::ExprListRep>;
    using SeqType = std::initializer_list<irtree::StmtRep>;

    TypeTable type_table_;
    ScopeTreeWalker st_walker_;
    FrameTreeWalker frame_tree_;
    ClassOffsets classes_offsets;

    irtree::IRTreeBuilder ir_tree_builder_;

    std::string current_class;

    ReturnType result;
    irtree::TempRep last_temp_label;

    void begin_scope() {
        st_walker_.begin_scope();
        frame_tree_.begin_scope();
    }

    void end_scope() {
        st_walker_.end_scope();
        frame_tree_.end_scope();
    }

public:

    IRTreeBuildVisitor(
                ScopeTreeWalker st_walker,
                FrameTreeWalker frame_tree,
                ClassOffsets offsets,
                TypeTable type_table
            ) :
            st_walker_(std::move(st_walker)),
            frame_tree_(std::move(frame_tree)),
            classes_offsets(std::move(offsets)),
            type_table_(type_table) {
    }

    static irtree::IRTree traverse(const ast::AST& ast, const TypeTable& type_table, const ScopeTreeWalker& st_walker) {
        auto classes_offset_ = ClassOffsets(type_table);
        auto frames = FrameTreeBuilder::get_offsets(st_walker, classes_offset_);

        auto visitor = IRTreeBuildVisitor(st_walker, frames, classes_offset_, type_table);

        visitor.rvisit(ast.code_);

        return std::move(visitor.ir_tree_builder_).build();
    }

private:

    irtree::MemRep get_variable_mem(const std::string& variable_name) {
        auto fp = irtree::Temp::get_fp();
        if (type_table_.get(current_class)->field(variable_name)) {
            auto size = classes_offsets.get_field_offset(current_class, variable_name);
            return util::create<irtree::Mem>(util::create<irtree::BinOpAdd>(
                    util::create<irtree::Mem>(fp), util::create<irtree::Const>(size)));
        } else {
            auto offset = frame_tree_.get_variable_offset(variable_name);
            return util::create<irtree::Mem>(util::create<irtree::BinOpAdd>(fp, util::create<irtree::Const>(offset)));
        }
    }

    template <typename T, typename U>
    T visit_with_value(U node) {
        rvisit(node);
        assert(std::holds_alternative<T>(result));
        return std::get<T>(result);
    }

    void visit(const ast::MethodCall &call) override {
        auto ret = visit_with_value<irtree::ExprListRep>(call.args);
        auto ir_call = util::create<irtree::Call>(call.obj_name, call.method_name, ret);

        last_temp_label = util::create<irtree::Temp>();
        result = util::create<irtree::Move>(ir_call, last_temp_label);
    }

    void visit(const ast::LvalueId &id) override {
        result = get_variable_mem(id.name);
    }

    void visit(const ast::LvalueField &field) override {
        rvisit(field.field);
    }

    void visit(const ast::LvalueArray &lvalueArray) override {
        auto ret = visit_with_value<irtree::ExprRep>(lvalueArray.expr);
        auto array_type = st_walker_.get_id(lvalueArray.name);
        auto ir_size = util::create<irtree::Const>(classes_offsets.get_object_size(array_type->name()));

        auto offset = util::create<irtree::BinOpAdd>(util::create<irtree::BinOpMul>(ret, ir_size),
                                                     util::create<irtree::Const>(SimpleTypeSize::INT));

        auto array_ptr = get_variable_mem(lvalueArray.name);
        auto elem_ptr = util::create<irtree::BinOpAdd>(array_ptr, offset);
        result = util::create<irtree::Mem>(elem_ptr);
    }

    void visit(const ast::Var &var) override {
        result = get_variable_mem(var.name);
    }

    void visit(const ast::Constant &constant) override {
        result = util::create<irtree::Const>(constant.val);
    }

    void visit(const ast::Decls &decls) override {
        for (const auto& decl : decls.decls) {
            rvisit(decl);
        }
    }

    void visit(const ast::StmtDeclVar &var) override {
        auto res = visit_with_value<irtree::ExprRep>(var.expr);
        auto var_ptr = get_variable_mem(var.name);
        result = util::create<irtree::StmtExpr>(util::create<irtree::Move>(res, var_ptr));
    }

    void visit(const ast::SimpleStmt &stmt) override {
        auto ret = visit_with_value<irtree::ExprRep>(stmt.expr);
        result = util::create<irtree::StmtExpr>(ret);
    }

    template <typename T>
    void handle_binop(ast::ExprRep ast_lhs, ast::ExprRep ast_rhs) {
        auto lhs = visit_with_value<irtree::ExprRep>(ast_lhs);
        auto rhs = visit_with_value<irtree::ExprRep>(ast_rhs);
        result = util::create<T>(lhs, rhs);
    }

    void visit(const ast::And &anAnd) override {
        handle_binop<irtree::BinOpAnd>(anAnd.lhs, anAnd.rhs);
    }

    void visit(const ast::Or &anOr) override {
        handle_binop<irtree::BinOpOr>(anOr.lhs, anOr.rhs);
    }

    void visit(const ast::Less &less) override {
        handle_binop<irtree::BinOpLess>(less.lhs, less.rhs);
    }

    void visit(const ast::Leq &leq) override {
        handle_binop<irtree::BinOpLeq>(leq.lhs, leq.rhs);
    }

    void visit(const ast::Greater &greater) override {
        handle_binop<irtree::BinOpGr>(greater.lhs, greater.rhs);
    }

    void visit(const ast::Geq &geq) override {
        handle_binop<irtree::BinOpGeq>(geq.lhs, geq.rhs);
    }

    void visit(const ast::IsEq &eq) override {
        handle_binop<irtree::BinOpIsEq>(eq.lhs, eq.rhs);
    }

    void visit(const ast::Mod &mod) override {
        handle_binop<irtree::BinOpMod>(mod.lhs, mod.rhs);
    }

    void visit(const ast::UNeg &neg) override {
        rvisit(neg.expr);
        assert(std::holds_alternative<irtree::ExprRep>(result));
        auto lhs = std::get<irtree::ExprRep>(result);
        result = util::create<irtree::UnOpNeg>(lhs);
    }

    void visit(const ast::UMinus &minus) override {
        auto ret = visit_with_value<irtree::ExprRep>(minus.expr);
        result = util::create<irtree::UnOpNeg>(ret);
    }

    void visit(const ast::Add &add) override {
        handle_binop<irtree::BinOpAdd>(add.lhs, add.rhs);
    }

    void visit(const ast::Diff &diff) override {
        handle_binop<irtree::BinOpSub>(diff.lhs, diff.rhs);
    }

    void visit(const ast::Product &product) override {
        handle_binop<irtree::BinOpMul>(product.lhs, product.rhs);
    }

    void visit(const ast::Slash &slash) override {
        handle_binop<irtree::BinOpDiv>(slash.lhs, slash.rhs);
    }

    void visit(const ast::Assign &assign) override {
        auto var_ptr = visit_with_value<irtree::ExprRep>(assign.var);
        auto res = visit_with_value<irtree::ExprRep>(assign.expr);
        result = util::create<irtree::Move>(res, var_ptr);
    }

    void visit(const ast::Exprs &exprs) override {
        auto res = util::create<irtree::ExprList>();
        for (const auto& it : exprs.exprs) {
            auto return_value = visit_with_value<irtree::ExprRep>(it);
            res = util::create<irtree::ExprList>(res, return_value);
        }
        result = res;
    }

    void visit(const ast::ExprEmpty &empty) override {
        // Empty expr return
        result = util::create<irtree::Const>(5);
    }

    void visit(const ast::FieldCall &call) override {
        if (call.field == "this") {
            auto field_offset = util::create<irtree::Const>(
                    classes_offsets.get_field_offset(current_class, call.object));
            auto fp = irtree::Temp::get_fp();
            auto field_ptr = util::create<irtree::BinOpAdd>(fp, field_offset);
            result = util::create<irtree::Mem>(field_ptr);
        } else {
            // TODO make public fields
            std::abort();
        }

    }

    void visit(const ast::Array &array1) override {
        auto ret = visit_with_value<irtree::ExprRep>(array1.index);
        auto array_type = st_walker_.get_id(array1.name);
        auto ir_size = util::create<irtree::Const>(classes_offsets.get_object_size(array_type->name()));

        auto offset = util::create<irtree::BinOpAdd>(util::create<irtree::BinOpMul>(ret, ir_size),
                util::create<irtree::Const>(SimpleTypeSize::INT));

        auto array_ptr = get_variable_mem(array1.name);
        auto elem_ptr = util::create<irtree::BinOpAdd>(array_ptr, offset);
        result = util::create<irtree::Mem>(elem_ptr);
    }

    void visit(const ast::Allocate &allocate) override {
        auto malloc_size = util::create<irtree::Const>(classes_offsets.get_object_size(allocate.type->name));
        auto args = util::create<irtree::ExprList>(std::initializer_list<irtree::ExprRep>{malloc_size});
        auto malloc_call = irtree::Call::malloc(args);

        last_temp_label = util::create<irtree::Temp>();
        result = util::create<irtree::Move>(malloc_call, last_temp_label);
    }

    void visit(const ast::AllocateArray &allocateArray) override {
        auto res = util::create<irtree::Temp>();
        auto size = util::create<irtree::Const>(classes_offsets.get_object_size(allocateArray.type->name));
        auto ret = visit_with_value<irtree::ExprRep>(allocateArray.quantity);

        auto malloc_size = util::create<irtree::BinOpAdd>(util::create<irtree::BinOpMul>(ret, size),
                util::create<irtree::Const>(SimpleTypeSize::INT));

        auto args = util::create<irtree::ExprList>(std::initializer_list<irtree::ExprRep>{malloc_size});
        auto malloc_call = irtree::Call::malloc(args);

        last_temp_label = util::create<irtree::Temp>();
        result = util::create<irtree::Move>(malloc_call, last_temp_label);
    }

    void visit(const ast::FieldArrayCall &call) override {

//            auto offset = classes_offsets.get_field_offset(call.object, call.field);

    }

    void visit(const ast::Return &aReturn) override {
        auto res = visit_with_value<irtree::ExprRep>(aReturn.expr);
        result = util::create<irtree::StmtExpr>(util::create<irtree::Move>(res, irtree::Temp::get_return()));
    }

    void visit(const ast::AccessMod &mod) override {
        // Nothing to do
    }

    void visit(const ast::Args &args) override {

    }

    void visit(const ast::Block &block) override {
        begin_scope();
        auto res = util::create<irtree::Seq>();
        for (const auto& it : block.stmts) {
            auto return_value = visit_with_value<irtree::StmtRep>(it);
            res = util::create<irtree::Seq>(res, return_value);
        }
        result = res;
        end_scope();
    }

    void visit(const ast::IfStmt &stmt) override {
        auto cond_expr = visit_with_value<irtree::ExprRep>(stmt.condition);

        auto true_stmt = visit_with_value<irtree::StmtRep>(stmt.then);

        auto false_stmt = visit_with_value<irtree::StmtRep>(stmt.otherwise);

        auto after = util::create<irtree::Label>();
        auto then = util::create<irtree::Label>();
        auto otherwise = util::create<irtree::Label>();

        auto cond_seq = util::create<irtree::CJump>(cond_expr, then, otherwise);

        auto true_seq = util::create<irtree::Seq>(SeqType {true_stmt, util::create<irtree::Jump>(after)});
        auto false_seq = util::create<irtree::Seq>(SeqType {false_stmt, util::create<irtree::Jump>(after)});

        auto true_branch = util::create<irtree::Seq>(SeqType {then, true_seq});
        auto false_branch = util::create<irtree::Seq>(SeqType {otherwise, false_seq});

        auto if_subtree = util::create<irtree::Seq>(SeqType {cond_seq, true_branch, false_branch, after});

        result = if_subtree;
    }

    void visit(const ast::For &aFor) override {
        begin_scope();
        auto init_stmt = visit_with_value<irtree::StmtRep>(aFor.init);
        auto cond_expr = visit_with_value<irtree::ExprRep>(aFor.condition);
        auto code_stmt = visit_with_value<irtree::StmtRep>(aFor.code);
        auto step_expr = visit_with_value<irtree::ExprRep>(aFor.step);

        auto step_stmt = util::create<irtree::StmtExpr>(step_expr);

        auto after = util::create<irtree::Label>();
        auto code = util::create<irtree::Label>();
        auto condition = util::create<irtree::Label>();
        auto cond_jmp = util::create<irtree::CJump>(cond_expr, code, after);

        auto jump_to_start = util::create<irtree::Jump>(condition);

        auto code_seq = util::create<irtree::Seq>(SeqType {code_stmt, util::create<irtree::Jump>(condition)});

        auto code_branch = util::create<irtree::Seq>(SeqType {code, code_seq});

        result = util::create<irtree::Seq>(SeqType {init_stmt, condition, cond_jmp, code, code_stmt, step_stmt, jump_to_start, after});

        end_scope();
    }

    void visit(const ast::While &aWhile) override {
        auto cond_expr = visit_with_value<irtree::ExprRep>(aWhile.condition);
        auto code_stmt = visit_with_value<irtree::StmtRep>(aWhile.code);

        auto after = util::create<irtree::Label>();
        auto code = util::create<irtree::Label>();
        auto condition = util::create<irtree::Label>();
        auto cond_jmp = util::create<irtree::CJump>(cond_expr, code, after);

        auto code_seq = util::create<irtree::Seq>(SeqType {code_stmt, util::create<irtree::Jump>(condition)});

        auto code_branch = util::create<irtree::Seq>(SeqType {code, code_seq});

        result = util::create<irtree::Seq>(SeqType {condition, cond_jmp, code_branch, after});
    }

    void visit(const ast::SimpleType &type) override {
        // Nothing to do
    }

    void visit(const ast::ArrayType &type) override {
        // Nothing to do
    }

    void visit(const ast::Class &aClass) override {
        begin_scope();
        current_class = aClass.class_name;
        rvisit(aClass.body);
        current_class.clear();
        end_scope();
    }

    void visit(const ast::Classes &classes) override {
        for (auto& decl : classes.decls) {
            rvisit(decl);
        }
    }

    void visit(const ast::ClassDeclMethod &method) override {
        const auto& method_name = method.id;
        const auto& method_label = current_class + "::" + method_name;
        const auto& label = util::create<irtree::Label>(method_label);
        auto code_ir = visit_with_value<irtree::StmtRep>(method.code);
        auto ir = util::create<irtree::Seq>(SeqType{label, code_ir});
        ir_tree_builder_.add(
                current_class,
                method_name,
                ir
            );
    }

    void visit(const ast::ClassDeclVar &var) override {
        // Nothing to do
    }
};
} // namespace visitor
