#pragma once

#include <list>
#include <map>

template<typename T, typename U>
std::list<T> get_keys(const std::unordered_map<T, U>& map) {
    std::list<T> keys;
    for (const auto& [k, _]: map)
        keys.push_back(k);
    return keys;
}