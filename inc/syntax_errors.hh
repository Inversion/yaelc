#pragma once

#include <string>
#include <sstream>
#include <list>

#include "location.hh"

namespace mini_java {
class SyntaxErrors;

class SyntaxError {
public:
    friend class mini_java::SyntaxErrors;

    SyntaxError(mini_java::location loc, std::string msg) :
        loc_(loc), msg_(std::move(msg)), discarded_() {}

    std::string msg() const {
        std::stringstream ss;
        ss  << discarded_ << ": Syntax error "
            << msg_ << " here " << loc_ << ";";

        return ss.str();
    }

private:
    mini_java::location loc_;
    std::string msg_;
    mini_java::location discarded_;
};

class SyntaxErrors {
public:
    void add(SyntaxError error) {
        errors_.emplace_back(std::move(error));
    }

    void update(mini_java::location loc) {
        errors_.back().discarded_ = std::move(loc);
    }

    bool has_error() {
        return !errors_.empty();
    }

    void print_errors(std::ostream& out) {
        for (const auto& error : errors_) {
            out << error.msg() << std::endl;
        }
    }
private:
    std::list<SyntaxError> errors_;
};
} // namespace mini_java
