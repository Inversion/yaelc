#pragma once

#include <iostream>
#include <string>

// I didn't get why we need to use guard
#if ! defined(yyFlexLexerOnce)
// Our lexer is minijavaFlexLexer
#define yyFlexLexer miniJavaFlexLexer
#include <FlexLexer.h>
#endif

// Say flex what function to implement
#undef YY_DECL
#define YY_DECL mini_java::parser::symbol_type mini_java::scanner::scanToken()

// generated
#include "mini_java_parser.hh"
#include "location.hh"

namespace mini_java {
class scanner: public miniJavaFlexLexer {
    mini_java::location loc;
public:
    void restart(
        const std::string& name,
        std::istream& input
    );

    virtual mini_java::parser::symbol_type scanToken();
};
} // namespace minijava