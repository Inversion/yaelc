#pragma once
#include <iostream>
#include <variant>
#include <memory>

namespace util {
    template<typename T>
    using Rep = std::shared_ptr<const T>;

    template<typename T, typename... Args>
    auto create(Args&&... args) -> Rep<T> {
        return std::make_shared<T, Args...>(std::forward<Args>(args)...);
    }

    template<typename T>
    auto get(const Rep<T>& rep) -> const T& {
        return *rep;
    };
}
