#pragma once

#include <iostream>
#include <list>
#include <memory>
#include <string>
#include <utility>
#include <variant>
#include <vector>

#include <modifiers.hh>
#include <ast/utils.hh>
#include <location.hh>

namespace ast {

using Value = std::variant<std::string, int, float, bool>;
using namespace mini_java;

class Visitor;

struct ASTNode {
  mini_java::location loc;
  virtual void accept(Visitor &visitor) const = 0;
  ASTNode(mini_java::location loc);
  virtual ~ASTNode() = 0;
};

struct ExprBase;
using ExprRep = util::Rep<ExprBase>;
struct ExprBase : public ASTNode {
  ExprBase(mini_java::location loc);
  void accept(Visitor &visitor) const override = 0;
  ~ExprBase() override = 0;
};

struct StmtBase;
using StmtRep = util::Rep<StmtBase>;
struct StmtBase : public ASTNode {
  void accept(Visitor &visitor) const override = 0;
  StmtBase(mini_java::location loc);
  ~StmtBase() override = 0;
};

struct ClassDeclBase;
using ClassDeclRep = util::Rep<ClassDeclBase>;
struct ClassDeclBase : public ASTNode {
  ClassDeclBase(mini_java::location loc);
  void accept(Visitor &visitor) const override = 0;
  ~ClassDeclBase() override = 0;
};

struct Decls;
using DeclsRep = util::Rep<Decls>;
struct Decls : public ASTNode {
  std::list<ClassDeclRep> decls;

  Decls(const mini_java::location& loc);
  Decls(const mini_java::location& loc, const ClassDeclRep &decl, const DeclsRep &other);

  void accept(Visitor &visitor) const override ;
};

struct Class;
using ClassRep = util::Rep<Class>;
struct Class : public ASTNode {
  std::string class_name;
  std::string base_class;
  DeclsRep body;

  Class(const mini_java::location& loc, std::string class_name, DeclsRep body);
  Class(const mini_java::location& loc, std::string class_name, std::string base_class, DeclsRep body);

  void accept(Visitor &visitor) const override ;
};

struct Classes;
using ClassesRep = util::Rep<Classes>;
struct Classes : public ASTNode {
  std::list<ClassRep> decls;

  Classes(const mini_java::location& loc);
  Classes(const mini_java::location& loc, const ClassRep &class_decl, const ClassesRep &other);
  void accept(Visitor &visitor) const override ;
};

struct ExprEmpty;
using ExprEmptyRep = util::Rep<ExprEmpty>;
struct ExprEmpty : ExprBase {
  ExprEmpty(const mini_java::location& loc);

  void accept(Visitor &visitor) const override;
};

struct SimpleStmt;
using SimpleStmtRep = util::Rep<SimpleStmt>;
struct SimpleStmt : StmtBase {
  ExprRep expr;

  SimpleStmt(const mini_java::location& loc, ExprRep expr);

  void accept(Visitor &visitor) const override;
};

struct SimpleType;
using TypeRep = util::Rep<SimpleType>;
struct SimpleType : public ASTNode {
  const std::string name;

  SimpleType(const mini_java::location& loc, std::string name);
  void accept(Visitor &visitor) const override;
};

struct ArrayType;
using ArrayTypeRep = util::Rep<ArrayType>;
struct ArrayType : public SimpleType {
  ArrayType(const mini_java::location& loc, TypeRep simpleType);
  void accept(Visitor &visitor) const override;
};

struct ClassDeclVar;
using ClassDeclVarRep = util::Rep<ClassDeclVar>;
struct ClassDeclVar : ClassDeclBase {
  const std::string name;
  ExprRep expr;
  TypeRep type;

  ClassDeclVar(const mini_java::location& loc, std::string name, ExprRep expr, TypeRep type);

  void accept(Visitor &visitor) const override;
};

struct StmtDeclVar;
using StmtDeclVarRep = util::Rep<StmtDeclVar>;
struct StmtDeclVar : StmtBase {
    const std::string name;
    ExprRep expr;
    TypeRep type;

    StmtDeclVar(const mini_java::location& loc, std::string name, ExprRep expr, TypeRep type);

    void accept(Visitor &visitor) const override;
};

struct Args;
using ArgsRep = util::Rep<Args>;
struct Args : public ASTNode {
  struct Arg {
    TypeRep type;
    const std::string name;

    Arg(TypeRep type, std::string name);
  };

  std::list<Arg> args;

  Args(const mini_java::location& loc);
  Args(const mini_java::location& loc, const std::string &var_name, const TypeRep &type, const ArgsRep &other);
  Args(const mini_java::location& loc, const std::string &var_name, const TypeRep &type);

  virtual void accept(Visitor &visitor) const override ;
};

struct Exprs;
using ExprsRep = util::Rep<Exprs>;
struct Exprs : public ASTNode{

  std::list<ExprRep> exprs;

  Exprs(const mini_java::location& loc);
  Exprs(const mini_java::location& loc, std::list<ExprRep> exprs_);
  Exprs(const mini_java::location& loc, const ExprRep &expr);
  Exprs(const mini_java::location& loc, const ExprRep &lhs, const ExprsRep &rhs);

  virtual void accept(Visitor &visitor) const override ;
};

struct Return;
using ReturnRep = util::Rep<Return>;
struct Return : StmtBase {
  ExprRep expr;

  Return(const mini_java::location& loc, ExprRep expr);

  void accept(Visitor &visitor) const override;
};

struct AllocateArray;
using AllocateArrayRep = util::Rep<AllocateArray>;
struct AllocateArray : public ExprBase {
  TypeRep type;
  ExprRep quantity;

  AllocateArray(const mini_java::location& loc, TypeRep type, ExprRep quantity);

  void accept(Visitor &visitor) const;
};

struct Allocate;
using AllocateRep = util::Rep<Allocate>;
struct Allocate : public ExprBase {
  TypeRep type;

  Allocate(const mini_java::location& loc, TypeRep type);

  void accept(Visitor &visitor) const override;
};

struct Array;
using ArrayRep = util::Rep<Array>;
struct Array : public ExprBase {
  std::string name;
  ExprRep index;

  Array(const mini_java::location& loc, const std::string &name, ExprRep index);

  void accept(Visitor &visitor) const override;
};

struct Var;
using VarRep = util::Rep<Var>;
struct Var : public ExprBase {
  const std::string name;

  Var(const mini_java::location& loc, std::string name);

  void accept(Visitor &visitor) const override;
};

struct MethodCall;
using MethodCallRep = util::Rep<MethodCall>;
struct MethodCall : public ExprBase {
  const std::string obj_name;
  const std::string method_name;
  ExprsRep args;

  MethodCall(const mini_java::location& loc, const std::string &obj, const std::string &method, ExprsRep exprs);

  void accept(Visitor &visitor) const override;
};

struct FieldCall;
using FieldCallRep = util::Rep<FieldCall>;
struct FieldCall : public ExprBase {
  std::string field;
  std::string object;
  FieldCall(const mini_java::location& loc, const std::string &object, const std::string &name);

  void accept(Visitor &visitor) const override;
};

struct FieldArrayCall;
using FieldArrayCallRep = util::Rep<FieldArrayCall>;
struct FieldArrayCall : public ExprBase {
    std::string field;
    std::string object;
    ExprRep expr;
    FieldArrayCall(const mini_java::location& loc, const std::string &object, const std::string &name, const ExprRep& expr_);
    void accept(Visitor &visitor) const override;
};

struct Block;
using BlockRep = util::Rep<Block>;
struct Block : public StmtBase {
  std::list<StmtRep> stmts;

  Block(const mini_java::location& loc);
  Block(const mini_java::location& loc, std::list<StmtRep> stmts_);
  Block(const location& loc, const StmtRep &stmt_, const BlockRep &other_);
  Block(const mini_java::location& loc, const BlockRep &other_);
  Block(const mini_java::location& loc, const StmtRep &stmt_);

  void accept(Visitor &visitor) const override;
};

struct AccessMod;
using AccessModRep = util::Rep<AccessMod>;
struct AccessMod : public ASTNode {
  mini_java::Modifiers modifier;

  AccessMod(const mini_java::location& loc, const mini_java::Modifiers &modifier);
  void accept(Visitor &visitor) const override;
};

struct ClassDeclMethod;
using ClassDeclMethodRep = util::Rep<ClassDeclMethod>;
struct ClassDeclMethod : ClassDeclBase {
  const std::string id;
  TypeRep type;
  ArgsRep args;
  AccessModRep modifier;
  BlockRep code;
  mini_java::Modifiers is_static;

  ClassDeclMethod(const mini_java::location& loc, std::string id, TypeRep type, ArgsRep args, AccessModRep modifier,
                  BlockRep code, const mini_java::Modifiers &is_static);

  void accept(Visitor &visitor) const override;
};

struct LvalueId;
using LvalueIdRep = util::Rep<LvalueId>;
struct LvalueId : ExprBase {
    std::string name;
    LvalueId(const mini_java::location& loc, const std::string name_);
    void accept(Visitor &visitor) const override;
};

struct LvalueArray;
using LvalueArrayRep = util::Rep<LvalueArray>;
struct LvalueArray : ExprBase {
    std::string name;
    ExprRep expr;
    LvalueArray(const mini_java::location& loc, const std::string name_, const ExprRep expr_);
    void accept(Visitor &visitor) const override;
};

struct LvalueField;
using LvalueFieldRep = util::Rep<LvalueField>;
struct LvalueField : ExprBase {
    ExprRep field;
    LvalueField(const mini_java::location& loc, const ExprRep& field_);
    void accept(Visitor &visitor) const override;
};

struct Assign;
using AssignRep = util::Rep<Assign>;
struct Assign : public ExprBase {
  ExprRep var;
  ExprRep expr;

  Assign(const mini_java::location& loc, ExprRep var_, ExprRep expr_);

  void accept(Visitor &visitor) const override;
};

struct IfStmt;
using IfStmtRep = util::Rep<IfStmt>;
struct IfStmt : public StmtBase {
  ExprRep condition;
  StmtRep then;
  StmtRep otherwise;

  IfStmt(const mini_java::location& loc, ExprRep condition_, StmtRep then_,
         StmtRep otherwise_);

  IfStmt(const mini_java::location& loc, ExprRep condition_, StmtRep then_);

  void accept(Visitor &visitor) const override;
};

struct For;
using ForRep = util::Rep<For>;
struct For : public StmtBase {
  StmtRep init;
  ExprRep step;
  ExprRep condition;
  StmtRep code;

  For(const mini_java::location& loc, StmtRep init_, ExprRep step_, ExprRep condition_, StmtRep code_);

  void accept(Visitor &visitor) const override;
};

struct While;
using WhileRep = util::Rep<While>;
struct While : public StmtBase {
  ExprRep condition;
  StmtRep code;

  While(const mini_java::location& loc, ExprRep condition_, StmtRep code_);

  void accept(Visitor &visitor) const override;
};

struct Constant;
using ConstantRep = util::Rep<Constant>;
struct Constant : public ExprBase {
  Value val;

  Constant(const mini_java::location& loc, Value val_);

  void accept(Visitor &visitor) const override;
};

enum OpID {
  AND,
  OR,
  LESS,
  LEQ,
  GREATER,
  GEQ,
  ISEQ,
  ADD,
  SUB,
  MUL,
  DIV,
  MOD,
  NEG,
};

template <int id> struct BinOp : public ExprBase {
  ExprRep lhs;
  ExprRep rhs;

  BinOp(const mini_java::location& loc, ExprRep lhs_, ExprRep rhs_);

  void accept(Visitor &visitor) const override;
};

template <int id> struct UnaryOp : public ExprBase {
  ExprRep expr;

  UnaryOp(const mini_java::location& loc, ExprRep expr);

  void accept(Visitor &visitor) const override;
};

using Add = BinOp<ADD>;
using AddRep = util::Rep<Add>;

using And = BinOp<AND>;
using AndRep = util::Rep<And>;

using Diff = BinOp<SUB>;
using DiffRep = util::Rep<Diff>;

using Geq = BinOp<GEQ>;
using GeqRep = util::Rep<Geq>;

using Greater = BinOp<GREATER>;
using GreaterRep = util::Rep<Greater>;

using IsEq = BinOp<ISEQ>;
using IsEqRep = util::Rep<IsEq>;

using Leq = BinOp<LEQ>;
using LeqRep = util::Rep<Leq>;

using Less = BinOp<LESS>;
using LessRep = util::Rep<Less>;

using Mod = BinOp<MOD>;
using ModRep = util::Rep<Mod>;

using UNeg = UnaryOp<NEG>;
using UNegRep = util::Rep<UNeg>;

using Or = BinOp<OR>;
using OrRep = util::Rep<Or>;

using Product = BinOp<MUL>;
using ProductRep = util::Rep<Product>;

using Slash = BinOp<DIV>;
using SlashRep = util::Rep<Slash>;

using UMinus = UnaryOp<SUB>;
using UMinusRep = util::Rep<UMinus>;

struct AST {
  ClassesRep code_;

  AST() = default;

  AST(ClassesRep code_);
};

class Visitor {
public:
  template <typename T> void rvisit(const util::Rep<T> &rep);
  
  virtual void visit(const ast::Assign &) = 0;
  virtual void visit(const ast::Exprs &) = 0;
  virtual void visit(const ast::ExprEmpty &) = 0;
  virtual void visit(const ast::FieldCall &) = 0;
  virtual void visit(const ast::Array &) = 0;
  virtual void visit(const ast::Allocate &) = 0;
  virtual void visit(const ast::AllocateArray &) = 0;
  virtual void visit(const ast::FieldArrayCall &) = 0;
  virtual void visit(const ast::Return &) = 0;
  virtual void visit(const ast::AccessMod &) = 0;
  virtual void visit(const ast::Args &) = 0;
  virtual void visit(const ast::Block &) = 0;
  virtual void visit(const ast::IfStmt &) = 0;
  virtual void visit(const ast::For &) = 0;
  virtual void visit(const ast::While &) = 0;
  virtual void visit(const ast::SimpleType &) = 0;
  virtual void visit(const ast::ArrayType &) = 0;
  virtual void visit(const ast::Class &) = 0;
  virtual void visit(const ast::Classes &) = 0;
  virtual void visit(const ast::ClassDeclMethod &) = 0;
  virtual void visit(const ast::ClassDeclVar &) = 0;
  virtual void visit(const ast::MethodCall &) = 0;
  virtual void visit(const ast::LvalueId &) = 0;
  virtual void visit(const ast::LvalueField &) = 0;
  virtual void visit(const ast::LvalueArray &) = 0;
  virtual void visit(const ast::Var &) = 0;
  virtual void visit(const ast::Constant &) = 0;
  virtual void visit(const ast::Decls &) = 0;
  virtual void visit(const ast::StmtDeclVar &) = 0;
  virtual void visit(const ast::SimpleStmt &) = 0;
  virtual void visit(const ast::And &) = 0;
  virtual void visit(const ast::Or &) = 0;
  virtual void visit(const ast::Less &) = 0;
  virtual void visit(const ast::Leq &) = 0;
  virtual void visit(const ast::Greater &) = 0;
  virtual void visit(const ast::Geq &) = 0;
  virtual void visit(const ast::IsEq &) = 0;
  virtual void visit(const ast::Mod &) = 0;
  virtual void visit(const ast::UNeg &) = 0;
  virtual void visit(const ast::UMinus &) = 0;
  virtual void visit(const ast::Add &) = 0;
  virtual void visit(const ast::Diff &) = 0;
  virtual void visit(const ast::Product &) = 0;
  virtual void visit(const ast::Slash &) = 0;
};
} // namespace ast

#include <ast/ast.ipp>