#pragma once

#include <iostream>
#include <typeinfo>


namespace ast {

inline ASTNode::~ASTNode() {}
inline StmtBase::~StmtBase() {}
inline ExprBase::~ExprBase() {}
inline ClassDeclBase::~ClassDeclBase() {}

inline ASTNode::ASTNode(mini_java::location loc) : loc(loc) {}

inline ExprBase::ExprBase(mini_java::location loc) : ASTNode(loc) {}
inline StmtBase::StmtBase(mini_java::location loc) : ASTNode(loc) {}
inline ClassDeclBase::ClassDeclBase(mini_java::location loc) : ASTNode(loc) {}

inline Classes::Classes(const mini_java::location& loc, const ClassRep &class_decl, const ClassesRep &other)
    : ASTNode(loc), decls(other->decls) {
  decls.push_front(class_decl);
};

inline Class::Class(const mini_java::location& loc, std::string class_name, DeclsRep body)
    : ASTNode(loc), class_name(std::move(class_name)), body(std::move(body)){};

inline Class::Class(const mini_java::location& loc, std::string class_name, std::string base_class,
                    DeclsRep body)
    : ASTNode(loc), class_name(std::move(class_name)), body(std::move(body)),
      base_class(std::move(base_class)){};

inline void Class::accept(Visitor &visitor) const { visitor.visit(*this); }

inline Decls::Decls(const mini_java::location& loc, const ClassDeclRep &decl, const DeclsRep &other)
    : ASTNode(loc), decls(other->decls) {
  decls.emplace_front(decl);
}

inline Decls::Decls(const mini_java::location& loc) : ASTNode(loc) {}

inline void Classes::accept(Visitor &visitor) const { visitor.visit(*this); }

inline Classes::Classes(const mini_java::location& loc) : ASTNode(loc) {}

inline void Decls::accept(Visitor &visitor) const { visitor.visit(*this); }

inline void ExprEmpty::accept(Visitor &visitor) const { visitor.visit(*this); }

inline ExprEmpty::ExprEmpty(const mini_java::location& loc) : ExprBase(loc) {}

inline SimpleStmt::SimpleStmt(const mini_java::location& loc, ExprRep expr) : StmtBase(loc), expr(expr) {}

inline void SimpleStmt::accept(Visitor &visitor) const { visitor.visit(*this); }

inline ClassDeclVar::ClassDeclVar(const mini_java::location& loc, std::string name, ExprRep expr, TypeRep type)
    : ClassDeclBase(loc), name(std::move(name)), expr(std::move(expr)), type(std::move(type)) {}

inline void ClassDeclVar::accept(Visitor &visitor) const { visitor.visit(*this); }

inline StmtDeclVar::StmtDeclVar(const mini_java::location& loc, std::string name, ExprRep expr, TypeRep type)
        : StmtBase(loc), name(std::move(name)), expr(std::move(expr)), type(std::move(type)) {}

inline void StmtDeclVar::accept(Visitor &visitor) const { visitor.visit(*this); }

inline Args::Arg::Arg(TypeRep type, std::string name)
        : type(std::move(type)), name(std::move(name)) {}

inline Args::Args(const mini_java::location& loc, const std::string &var_name, const TypeRep &type,
                  const ArgsRep &other)
    : ASTNode(loc), args(other->args) {
  args.emplace_front(type, var_name);
}

inline Args::Args(const mini_java::location& loc) : ASTNode(loc) {}

inline Args::Args(const mini_java::location& loc, const std::string &var_name, const TypeRep &type) : ASTNode(loc) {
  args.emplace_front(type, var_name);
}

inline void Args::accept(Visitor &visitor) const { visitor.visit(*this); }

inline Exprs::Exprs(const mini_java::location& loc, std::list<ExprRep> exprs_) : ASTNode(loc), exprs(std::move(exprs_)) {}

inline Exprs::Exprs(const mini_java::location& loc, const ExprRep &expr) : ASTNode(loc) { exprs.push_front(expr); }

inline Exprs::Exprs(const mini_java::location& loc) : ASTNode(loc) {}

inline Exprs::Exprs(const mini_java::location& loc, const ExprRep &lhs, const ExprsRep &rhs)
    : ASTNode(loc), exprs(rhs->exprs) {
  exprs.push_front(lhs);
}

inline void Exprs::accept(Visitor &visitor) const { visitor.visit(*this); }

inline Return::Return(const mini_java::location& loc, ExprRep expr) : StmtBase(loc), expr(std::move(expr)){};

inline void Return::accept(Visitor &visitor) const { visitor.visit(*this); }

inline AllocateArray::AllocateArray(const mini_java::location& loc, TypeRep type, ExprRep quantity)
    : ExprBase(loc), type(std::move(type)), quantity(std::move(quantity)) {}

inline void AllocateArray::accept(Visitor &visitor) const {
  visitor.visit(*this);
}

inline Allocate::Allocate(const mini_java::location& loc, TypeRep type) : ExprBase(loc), type(std::move(type)) {}

inline void Allocate::accept(Visitor &visitor) const { visitor.visit(*this); }

inline Array::Array(const mini_java::location& loc, const std::string &name, ExprRep index)
    : ExprBase(loc), name(std::move(name)), index(std::move(index)) {}

inline void Array::accept(Visitor &visitor) const { visitor.visit(*this); }

inline Var::Var(const mini_java::location& loc, std::string name) : ExprBase(loc), name(std::move(name)) {}

inline void Var::accept(Visitor &visitor) const { visitor.visit(*this); }

inline MethodCall::MethodCall(const mini_java::location& loc, const std::string &obj, const std::string &method,
                              ExprsRep exprs)
    : ExprBase(loc), obj_name(obj), method_name(std::move(method)), args(std::move(exprs)) {
}

inline void MethodCall::accept(Visitor &visitor) const { visitor.visit(*this); }

inline FieldCall::FieldCall(const mini_java::location& loc, const std::string &object, const std::string &name)
    : ExprBase(loc), field(name), object(object) {}

inline void FieldCall::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

inline FieldArrayCall::FieldArrayCall(const mini_java::location& loc, const std::string &object, const std::string &name, const ast::ExprRep& expr)
    : ExprBase(loc), object(object), field(name), expr(expr) {}

inline void FieldArrayCall::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

inline ClassDeclMethod::ClassDeclMethod(const mini_java::location& loc, std::string id, TypeRep type, ArgsRep args,
                                    AccessModRep modifier, BlockRep code,
                                    const mini_java::Modifiers &is_static)
    : ClassDeclBase(loc), id(std::move(id)), type(std::move(type)), args(std::move(args)),
      modifier(std::move(modifier)), code(std::move(code)),
      is_static(is_static) {}

inline void ClassDeclMethod::accept(Visitor &visitor) const { visitor.visit(*this); }

inline AccessMod::AccessMod(const mini_java::location& loc, const mini_java::Modifiers &modifier)
        : ASTNode(loc), modifier(modifier) {}

inline void AccessMod::accept(Visitor &Visitor) const { Visitor.visit(*this); }

inline SimpleType::SimpleType(const mini_java::location& loc, std::string name)
: ASTNode(loc), name(std::move(name)) {}

inline void SimpleType::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

inline ArrayType::ArrayType(const mini_java::location& loc_, TypeRep simpleType)
    : SimpleType(util::get(simpleType)) {loc = loc_;}

inline void ArrayType::accept(Visitor &visitor) const { visitor.visit(*this); }

inline LvalueId::LvalueId(const mini_java::location& loc, const std::string name_) : ExprBase(loc), name(std::move(name_)){}

inline void LvalueId::accept(Visitor &visitor) const { visitor.visit(*this); }

inline LvalueArray::LvalueArray(const mini_java::location& loc, const std::string name_, const ExprRep expr_)
    : ExprBase(loc), name(std::move(name_)), expr(std::move(expr_)){}

inline void LvalueArray::accept(Visitor &visitor) const { visitor.visit(*this); }

inline LvalueField::LvalueField(const mini_java::location& loc, const ExprRep& field_) : ExprBase(loc), field(field_){}

inline void LvalueField::accept(Visitor &visitor) const { visitor.visit(*this); }

inline Assign::Assign(const mini_java::location& loc, ExprRep var_, ExprRep expr_)
    : ExprBase(loc), var(std::move(var_)), expr(std::move(expr_)) {}

inline void Assign::accept(Visitor &visitor) const { visitor.visit(*this); }

inline Block::Block(const mini_java::location& loc, std::list<StmtRep> stmts_) : StmtBase(loc), stmts(std::move(stmts_)) {}

inline Block::Block(const mini_java::location& loc) : StmtBase(loc) {}

inline Block::Block(const mini_java::location& loc, const StmtRep &stmt_, const BlockRep &other_)
    : StmtBase(loc), stmts(other_->stmts) {
  stmts.push_front(stmt_);
}

inline Block::Block(const mini_java::location& loc, const BlockRep &other_) : StmtBase(loc), stmts(other_->stmts) {}

inline Block::Block(const mini_java::location& loc, const StmtRep &stmt_) : StmtBase(loc){ stmts.push_front(stmt_); }

inline void Block::accept(Visitor &visitor) const { visitor.visit(*this); }

inline IfStmt::IfStmt(const mini_java::location& loc, ExprRep condition_, StmtRep then_, StmtRep otherwise_)
    : StmtBase(loc), condition(std::move(condition_)), then(std::move(then_)),
      otherwise(std::move(otherwise_)) {}

inline IfStmt::IfStmt(const mini_java::location& loc, ExprRep condition_, StmtRep then_)
    : StmtBase(loc), condition(std::move(condition_)), then(std::move(then_)),
      otherwise(util::create<Block>(loc)) {}


inline void IfStmt::accept(Visitor &visitor) const { visitor.visit(*this); }

inline For::For(const mini_java::location& loc, StmtRep init_, ExprRep step_, ExprRep condition_, StmtRep code_)
    : StmtBase(loc), init(std::move(init_)), step(std::move(step_)),
      condition(std::move(condition_)), code(std::move(code_)) {}

inline void For::accept(Visitor &visitor) const { visitor.visit(*this); }

inline While::While(const mini_java::location& loc, ExprRep condition_, StmtRep code_)
    : StmtBase(loc), condition(std::move(condition_)), code(std::move(code_)) {}

inline void While::accept(Visitor &visitor) const { visitor.visit(*this); }

inline Constant::Constant(const mini_java::location& loc, Value val_) : ExprBase(loc), val(std::move(val_)) {}

inline void Constant::accept(Visitor &visitor) const { visitor.visit(*this); }

template <int id>
inline BinOp<id>::BinOp(const mini_java::location& loc, ExprRep lhs_, ExprRep rhs_)
    : ExprBase(loc), lhs(std::move(lhs_)), rhs(std::move(rhs_)) {}

template <int id> inline void BinOp<id>::accept(Visitor &visitor) const {
  visitor.visit(*this);
}

template <int id>
inline UnaryOp<id>::UnaryOp(const mini_java::location& loc, ExprRep expr) : ExprBase(loc), expr(std::move(expr)) {}

template <int id> inline void UnaryOp<id>::accept(Visitor &visitor) const {
  visitor.visit(*this);
}

inline AST::AST(ClassesRep code_) : code_(std::move(code_)) {}

template <typename T> void Visitor::rvisit(const util::Rep<T> &rep) {
    if (!rep) {
        std::cerr << "Encountered null rep" << std::endl;
        std::abort();
    }
    rep->accept(*this);
//    else visit(util::get(rep));
}
} // namespace ast