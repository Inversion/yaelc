#pragma once

#include <string>
#include <vector>

#include <ast/utils.hh>
#include <irtree/generators.hpp>

#include <helpers.hh>

namespace irtree {
using namespace util;
using Value = std::variant<std::string, int, float, bool>;

class Visitor;

struct IRNode {
    virtual ~IRNode() = default;

    virtual void accept(Visitor &visitor) const = 0;
};

using IRNodeRep = util::Rep<IRNode>;

struct StmtBase : IRNode {
    ~StmtBase() override = default;
};
using StmtRep = util::Rep<StmtBase>;

struct ExprBase : IRNode {
    ~ExprBase() override = default;
};
using ExprRep = util::Rep<ExprBase>;

struct ExprList;
using ExprListRep = util::Rep<ExprList>;
struct ExprList : IRNode {
    std::vector<ExprRep> exprs;
    ExprList(std::initializer_list<ExprRep> exprs);
    ExprList(ExprListRep prev, ExprRep next);
    ExprList() = default;
    void accept(Visitor &visitor) const override;
};

struct StmtExpr : public StmtBase {
    ExprRep expr;

    void accept(Visitor &visitor) const override;
    StmtExpr(const ExprRep &expr);
};
using StmtExprRep = util::Rep<StmtExpr>;

struct Temp : ExprBase {
    TempLabel label_;
    void accept(Visitor &visitor) const override;
    Temp() = default;

    static auto get_return();
    static auto get_fp();

    Temp(std::string label_name);
};
using TempRep = util::Rep<Temp>;

struct Call : ExprBase {
    std::string object;
    std::string method;
    ExprListRep args;
    void accept(Visitor &visitor) const override;

    Call(const string &object, const string &method, const ExprListRep &args);
    static auto malloc(const ExprListRep &args);

};
using CallRep = util::Rep<Call>;

struct Eseq : ExprBase {
    StmtRep stmt;
    ExprRep expr;
    void accept(Visitor &visitor) const override;

    Eseq(const StmtRep &stmt, const ExprRep &expr);
};
using EseqRep = util::Rep<Eseq>;

struct Mem : ExprBase {
    ExprRep address;
    void accept(Visitor &visitor) const override;

    Mem(const ExprRep &address);

};
using MemRep = util::Rep<Mem>;

struct Name : ExprBase {
    CodeLabel label;
    void accept(Visitor &visitor) const override;
};

struct Const : ExprBase {
    Value value;
    void accept(Visitor &visitor) const override;
    template <typename T>
    Const(T val);
};
using ConstRep = util::Rep<Const>;

using NameRep = util::Rep<Name>;

enum OpID {
    AND,
    OR,
    LESS,
    LEQ,
    GR,
    GEQ,
    ISEQ,
    ADD,
    SUB,
    MUL,
    DIV,
    MOD,
    NEG,
};


template <int id>
struct BinOp : ExprBase {
    ExprRep lhs;
    ExprRep rhs;
    void accept(Visitor &visitor) const override;

    BinOp(const ExprRep &lhs, const ExprRep &rhs);
};
template <int id>
using BinOpRep = util::Rep<BinOp<id>>;

using BinOpAdd = BinOp<ADD>;
using BinOpMul = BinOp<MUL>;
using BinOpDiv = BinOp<DIV>;
using BinOpAnd = BinOp<AND>;
using BinOpOr = BinOp<OR>;
using BinOpLess = BinOp<LESS>;
using BinOpLeq = BinOp<LEQ>;
using BinOpGr = BinOp<GR>;
using BinOpGeq = BinOp<GEQ>;
using BinOpIsEq = BinOp<ISEQ>;
using BinOpSub = BinOp<SUB>;
using BinOpMod = BinOp<MOD>;

template <int id>
struct UnOp : ExprBase {
    ExprRep lhs;
    void accept(Visitor &visitor) const override;

    UnOp(const ExprRep &lhs);
};
template <int id>
using UnOpRep = util::Rep<UnOp<id>>;

using UnOpNeg = UnOp<NEG>;
using UnOpSub = UnOp<SUB>;


struct Move : ExprBase {
    ExprRep source;
    ExprRep dest;
    void accept(Visitor &visitor) const override;

    Move(const ExprRep &source, const ExprRep &dest);
};

struct Label : StmtBase {
    CodeLabel label_name;
    void accept(Visitor &visitor) const override;

    Label(const std::string &labelName);
    Label() = default;
};

using LabelRep = util::Rep<Label>;

struct Jump : StmtBase {
    LabelRep jump_to;
    void accept(Visitor &visitor) const override;
    Jump(const LabelRep &jumpTo);
};

struct CJump : StmtBase {
    ExprRep bin_op;
    LabelRep jump_true;
    LabelRep jump_false;
    void accept(Visitor &visitor) const override;
    CJump(const ExprRep &binOp, const LabelRep &jumpTrue, const LabelRep &jumpFalse);
};

struct Seq;
using SeqRep = util::Rep<Seq>;
struct Seq : StmtBase {
    std::vector<StmtRep> stmts;
    Seq(std::initializer_list<StmtRep> stmts);
    Seq(SeqRep prev, StmtRep next);
    Seq() = default;
    void accept(Visitor &visitor) const override;
};

class IRTreeBuilder;

class IRTree {
    using ClassIRMap = std::unordered_map<std::string, SeqRep>;
    using IRMap = std::unordered_map<std::string, ClassIRMap>;
    IRMap ir_;

    explicit IRTree(IRMap&& map) : ir_(std::move(map)) {}
public:
    SeqRep get_ir_for(const std::string& class_name, const std::string& method_name) const {
        return ir_.at(class_name).at(method_name);
    }

    std::list<std::string> get_classes() const {
        return get_keys(ir_);
    }

    std::list<std::string> get_methods(const std::string& class_name) const {
        return get_keys(ir_.at(class_name));
    }

    friend IRTreeBuilder;
private:

};

class IRTreeBuilder {
    using ClassIRMap = std::unordered_map<std::string, SeqRep>;
    using IRMap = std::unordered_map<std::string, ClassIRMap>;
    IRMap ir_;

public:
    IRTreeBuilder() = default;

    void add(const std::string& class_name, const std::string& method_name, SeqRep ir) {
        ir_[class_name][method_name] = std::move(ir);
    }

    IRTree build() && {
        auto ir = std::move(ir_);
        return IRTree(std::move(ir));
    }
};



class Visitor {
public:
    template <typename T> void rvisit(const util::Rep<T> &rep);

    virtual void visit(const irtree::ExprList &) = 0;
    virtual void visit(const irtree::Temp &) = 0;
    virtual void visit(const irtree::Call &) = 0;
    virtual void visit(const irtree::Eseq &) = 0;
    virtual void visit(const irtree::Mem &) = 0;
    virtual void visit(const irtree::Name &) = 0;
    virtual void visit(const irtree::Const &) = 0;
    virtual void visit(const irtree::BinOpAdd &) = 0;
    virtual void visit(const irtree::BinOpMul &) = 0;
    virtual void visit(const irtree::BinOpDiv &) = 0;
    virtual void visit(const irtree::BinOpAnd &) = 0;
    virtual void visit(const irtree::BinOpOr &) = 0;
    virtual void visit(const irtree::BinOpLess &) = 0;
    virtual void visit(const irtree::BinOpLeq &) = 0;
    virtual void visit(const irtree::BinOpGr &) = 0;
    virtual void visit(const irtree::BinOpGeq &) = 0;
    virtual void visit(const irtree::BinOpIsEq &) = 0;
    virtual void visit(const irtree::BinOpSub &) = 0;
    virtual void visit(const irtree::BinOpMod &) = 0;
    virtual void visit(const irtree::UnOpNeg &) = 0;
    virtual void visit(const irtree::UnOpSub &) = 0;
    virtual void visit(const irtree::Move &) = 0;
    virtual void visit(const irtree::Label &) = 0;
    virtual void visit(const irtree::Jump &) = 0;
    virtual void visit(const irtree::CJump &) = 0;
    virtual void visit(const irtree::Seq &) = 0;
    virtual void visit(const irtree::StmtExpr &) = 0;
};

#include <irtree/irtree.ipp>
}