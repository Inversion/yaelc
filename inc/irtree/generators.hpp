#pragma once
#include <string>

namespace irtree {
template <char t>
class Counter {
public:
    Counter() {
        label_ = std::string(1, t) + std::to_string(counter_++);
    }

    Counter(std::string label) : label_(std::move(label)) {}

    std::string to_string() const {
        return label_;
    }

private:
    std::string label_;
    static int counter_;
};
template <char t>
int Counter<t>::counter_;

using CodeLabel = Counter<'L'>;
using TempLabel = Counter<'T'>;
}