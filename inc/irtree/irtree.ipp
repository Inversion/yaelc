#pragma once

inline Seq::Seq(SeqRep prev, StmtRep next) : stmts(prev->stmts) {
    stmts.emplace_back(next);
}

inline Seq::Seq(std::initializer_list<StmtRep> stmts) : stmts(stmts) {}

inline void Seq::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

inline CJump::CJump(const ExprRep &binOp, const LabelRep &jumpTrue, const LabelRep &jumpFalse) :
                    bin_op(binOp), jump_true(jumpTrue), jump_false(jumpFalse) {}

inline void CJump::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

inline Jump::Jump(const LabelRep &jumpTo) : jump_to(jumpTo) {}

inline void Jump::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

inline Move::Move(const ExprRep &source, const ExprRep &dest) : source(source), dest(dest) {}

inline void Move::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

template <int id>
inline UnOp<id>::UnOp(const ExprRep &lhs) : lhs(lhs) {}

template <int id>
inline void UnOp<id>::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

template <int id>
inline BinOp<id>::BinOp(const ExprRep &lhs, const ExprRep &rhs) : lhs(lhs), rhs(rhs) {}

template <int id>
inline void BinOp<id>::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

template <typename T>
inline Const::Const(T val) : value(val) {}

inline void Const::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

inline void Mem::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

inline Mem::Mem(const ExprRep &address) : address(address) {}

inline void Eseq::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

inline Eseq::Eseq(const StmtRep &stmt, const ExprRep &expr) : expr(expr), stmt(stmt) {}

inline void Call::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

void Label::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

Label::Label(const std::string &labelName) : label_name(labelName) {}

inline Call::Call(const string &object, const string &method, const ExprListRep &args) : object(object), method(method),
                                                                            args(args) {}

inline void Temp::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

inline Temp::Temp(std::string label_name) : label_(label_name) {};

inline void ExprList::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

inline ExprList::ExprList(ExprListRep prev, ExprRep next) : exprs(prev->exprs) {
    exprs.emplace_back(next);
}

inline ExprList::ExprList(std::initializer_list<ExprRep> exprs) : exprs(exprs) {}

StmtExpr::StmtExpr(const ExprRep &expr) : expr(expr) {}
inline void StmtExpr::accept(Visitor &visitor) const {
    visitor.visit(*this);
}

auto Call::malloc(const ExprListRep &args) {
    return util::create<Call>("System", "malloc", args);
}

auto Temp::get_return() {
    return util::create<Temp>("return_label");
}

auto Temp::get_fp() {
    return util::create<Temp>("f_ptr");
}
void Name::accept(Visitor &visitor) const {
    visitor.visit(*this);
};

template <typename T> void Visitor::rvisit(const util::Rep<T> &rep) {
    if (!rep) {
        std::cerr << "Encountered null rep" << std::endl;
        std::abort();
    }
    rep->accept(*this);
}