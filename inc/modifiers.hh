#pragma once

namespace mini_java {
    enum class Modifiers {
        PUBLIC,
        PRIVATE,
        PROTECTED,
        STATIC,
        NOTSTATIC,
        DEFAULT
    };
}
