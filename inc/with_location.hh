#pragma once

#include <location.hh>

namespace mini_java {
template<typename T>
struct WithLoc {
    T val;
    location loc;
};
}