#pragma once

#include <frames/frame.hh>

namespace mini_java {
    class FrameTreeWalker {
    private:
        std::shared_ptr<Frame> root_;
        std::shared_ptr<Frame> current_scope;
        std::stack<int> scope_num;

    public:
        FrameTreeWalker(const std::shared_ptr<Frame>& root): current_scope(root), root_(root) {
            scope_num.push(0);
        }

        void begin_scope() {
            current_scope = current_scope->children[scope_num.top()++];
            scope_num.push(0);
        }

        void end_scope() {
            current_scope = current_scope->parent.lock();
            scope_num.pop();
        }

        int get_variable_offset(const std::string& variable) {
            auto tmp_scope = current_scope;
            while (tmp_scope != root_) {
                if (tmp_scope->offsets.count(variable)) {
                    return tmp_scope->offsets.at(variable).offset;
                }
                tmp_scope = tmp_scope->parent.lock();
            }
            //error if variable not exist
            std::abort();
        }
    };
}