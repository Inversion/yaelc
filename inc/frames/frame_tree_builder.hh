#pragma once

#include <frames/frame.hh>
#include <frames/frame_tree_walker.hh>
#include <utility>
#include <frames/class_offsets.hh>

namespace mini_java {

class FrameTreeBuilder {
    std::shared_ptr<Frame> frame_tree;
    mini_java::ScopeTreeWalker st_walker_;
    std::stack<int> current_offset;
    const ClassOffsets classes_info_;

    std::shared_ptr<Frame> open_frame(const std::shared_ptr<Frame>& ptr) {
        st_walker_.begin_scope();
        current_offset.push(current_offset.top());
        auto new_frame = std::make_shared<Frame>(ptr);
        ptr->children.push_back(new_frame);
        return new_frame;
    }

    std::shared_ptr<Frame> close_frame(const std::shared_ptr<Frame>& ptr) {
        st_walker_.end_scope();
        current_offset.pop();
        return ptr->parent.lock();
    }

    void insert_frame_offset(const std::shared_ptr<Frame>& frame,
                             std::shared_ptr<const mini_java::Type> type,
                             std::string name) {
        auto size = classes_info_.get_type_size(std::move(type));
        frame->offsets[name] = {.offset = current_offset.top(),
                                .size = size};
        current_offset.top() += size;
    }

    void traverse(const std::shared_ptr<Frame>& frame) {
        int classes_num = st_walker_.children_size();
        for (int i = 0; i < classes_num; i++) {
            auto new_frame = open_frame(frame);
            auto fields = st_walker_.get_ids();
            for (auto [field_name, field] : fields) {
                insert_frame_offset(new_frame, field.type, field_name);
            }
            traverse(new_frame);
            close_frame(new_frame);
        }
    }

    void build_frame_tree() {
        int classes_num = st_walker_.children_size();
        current_offset.push(0);
        for (int i = 0; i < classes_num; i++) {
            auto new_frame = open_frame(frame_tree);

            traverse(new_frame);

            close_frame(new_frame);
        }
    }

    FrameTreeBuilder(
            mini_java::ScopeTreeWalker st_walker,
            mini_java::ClassOffsets classes_info
        ) :
        st_walker_(std::move(st_walker)),
        classes_info_(std::move(classes_info)) {
        frame_tree = std::make_shared<Frame>(nullptr);
    }

public:

    static FrameTreeWalker get_offsets(
            mini_java::ScopeTreeWalker st_walker,
            mini_java::ClassOffsets class_offsets
        ) {
        FrameTreeBuilder frame_tree(std::move(st_walker), std::move(class_offsets));
        frame_tree.build_frame_tree();
        return FrameTreeWalker(frame_tree.frame_tree);
    }
};
}