#pragma once

#include <frames/frame.hh>

namespace mini_java {
class ClassOffsets {
public:
    int get_field_offset(const std::string &class_name, const std::string &field_name) const {
        return offsets_.at(class_name).fields.at(field_name).offset;
    }

    int get_field_size(const std::string &class_name, const std::string &field_name) const {
        return offsets_.at(class_name).fields.at(field_name).size;
    }

    int get_object_size(const std::string &class_name) const {
        return offsets_.at(class_name).size;
    }

    int get_type_size(const std::shared_ptr<const mini_java::Type>& type) const {
        return type_size(type);
    }

    explicit ClassOffsets(const mini_java::TypeTable& types) {
        for (auto [class_name, class_type] : types.types) {
            auto class_t = std::dynamic_pointer_cast<ClassType>(class_type);
            ClassSize current_class = {};
            if (class_t) {
                for (auto [field_name, field_type] : class_t->fields()) {
                    auto cur_size = type_size(field_type);
                    current_class.fields[field_name] = {
                        .offset = current_class.size,
                        .size = cur_size
                    };
                    current_class.size += cur_size;
                }
            } else {
                current_class.size = type_size(class_type);
            }
            offsets_[class_name] = current_class;
        }
    }

private:
    struct ClassSize {
        std::map<std::string, SizeInfo> fields;
        int size;
    };
    std::map<std::string, ClassSize> offsets_;

    static int type_size(const std::shared_ptr<const mini_java::Type>& type)  {
        // Store other types on heap
        int res_size = SimpleTypeSize::PTR;
        if (type->name() == mini_java::IntType::NAME) {
            res_size = SimpleTypeSize::INT;
        } else if (type->name() == mini_java::CharType::NAME) {
            res_size = SimpleTypeSize::CHAR;
        } else if (type->name() == mini_java::FloatType::NAME) {
            res_size = SimpleTypeSize::FLOAT;
        } else if (type->name() == mini_java::DoubleType::NAME) {
            res_size = SimpleTypeSize::DOUBLE;
        }
        if (res_size < SimpleTypeSize::ALIGN) {
            res_size = SimpleTypeSize::ALIGN;
        }
        return res_size;
    }
};
}