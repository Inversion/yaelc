#pragma once

#include <scopes/scope_tree.hh>
#include <scopes/scope_tree_walker.hh>
#include <map>
#include <types/type.hh>
#include <utility>


namespace mini_java {
namespace SimpleTypeSize {
    const int INT = 4;
    const int CHAR = 1;
    const int PTR = 4;
    const int DOUBLE = 8;
    const int FLOAT = 4;
    const int ALIGN = 4;
};

struct SizeInfo {
    int offset;
    int size;
};

struct Frame {
    std::vector<std::shared_ptr<Frame>> children;
    std::weak_ptr<Frame> parent;
    std::map<std::string, SizeInfo> offsets;

    Frame(std::shared_ptr<Frame> parent) : parent(parent) {}
};
}
