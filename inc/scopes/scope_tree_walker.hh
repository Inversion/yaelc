#pragma once

#include <scopes/scope_tree.hh>
namespace mini_java {
class ScopeTreeWalker {
    ScopeTree tree_;
    std::stack<int> scope_num;
    std::shared_ptr<ScopeTree::ScopeNode> current_scope;
public:
    ScopeTreeWalker(ScopeTree&& tree) : tree_(tree) {
        current_scope = tree_.root_node;
        scope_num.push(0);
    }

    void begin_scope() {
        current_scope = current_scope->child_scopes[scope_num.top()++];
        scope_num.push(0);
    }

    const auto& get_ids() {
        return current_scope->identifiers;
    }

    auto get_id(const std::string& id) {
        return tree_.get_type(current_scope, id);
    }

    const auto& get_methods() {
        return current_scope->methods;
    }

    std::string scope_name() {
        return current_scope->scope_name;
    }

    int children_size() {
        return current_scope->child_scopes.size();
    }

    void end_scope() {
        current_scope = current_scope->parent_scope.lock();
        scope_num.pop();
    }
};
}
