#pragma once

#include <types/type.hh>

#include <unordered_map>
#include <stack>
#include <string>
#include <memory>
#include <utility>

namespace mini_java {

class ScopeTreeBuilder;
class ScopeTreeWalker;

class ScopeTree {
public:
    struct ScopeSymbol {
        std::shared_ptr<const Type> type;
        std::string name;
    };

    struct ScopeMethod {
        std::shared_ptr<const Method> method;
        std::string name;
    };

private:
    class ScopeNode {
        friend ScopeTree;
        friend ScopeTreeBuilder;
        friend ScopeTreeWalker;

        using IdMap = std::unordered_map <std::string, ScopeSymbol>;
        using MethodMap = std::unordered_map <std::string, ScopeMethod>;

        std::weak_ptr<ScopeNode> parent_scope;
        std::vector<std::shared_ptr<ScopeNode>> child_scopes;
        std::string scope_name;

        IdMap identifiers;
        MethodMap methods;

    public:

        ScopeNode(std::weak_ptr<ScopeNode> parent) :
                parent_scope(std::move(parent)) {}
    };

    std::shared_ptr<ScopeNode> root_node;

    ScopeTree() :
            root_node(std::make_shared<ScopeNode>(std::weak_ptr<ScopeNode>())) {}

    std::shared_ptr<const Type> get_type(
            std::shared_ptr<ScopeNode> node,
            const std::string& name
    ) {
        std::shared_ptr<const Type> type = nullptr;

        if (node->identifiers.count(name)) {
            type = node->identifiers.at(name).type;
        } else do {
                node = node->parent_scope.lock();
                if (node->identifiers.count(name)) {
                    type = node->identifiers.at(name).type;
                    break;
                }
            } while (node != root_node);

        return type;
    }

    friend ScopeTreeBuilder;
    friend ScopeTreeWalker;
};

}
