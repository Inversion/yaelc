#pragma once

#include <types/type.hh>
#include <scopes/scope_tree.hh>
#include <scopes/scope_tree_walker.hh>

#include <stack>
#include <cassert>
#include <utility>

namespace mini_java {

class ScopeTreeBuilder {
    using ScopeNode = ScopeTree::ScopeNode;

    ScopeTree tree;
    std::shared_ptr<ScopeNode> current_node;
public:

    ScopeTreeBuilder() {
        current_node = tree.root_node;
    }

    void begin_scope() {
        auto child = std::make_shared<ScopeNode>(current_node);
        current_node->child_scopes.emplace_back(child);
        current_node = child;
    }

    void begin_class(std::string class_name) {
        current_node->scope_name = std::move(class_name);
        begin_scope();
    }

    void end_scope() {
        current_node = current_node->parent_scope.lock();
    }

    void end_class() {
        end_scope();
    }

    void insert_variable(const std::string& name, std::shared_ptr<const Type> type) {
        ScopeTree::ScopeSymbol symbol {
                .type = type,
                .name = name
        };

        current_node->identifiers[symbol.name] = symbol;
    }

    void insert_method(const std::string& name, std::shared_ptr<const Method> type) {
        ScopeTree::ScopeMethod method {
            .method = std::move(type),
            .name = name
        };
        current_node->methods[method.name] = method;
    }

    ScopeTree get_tree() && {
        auto retval = std::move(tree);
        return retval;
    }

    bool check_this_scope_variable(const std::string& name) {
        return current_node->identifiers.count(name);
    }

    bool check_variable(const std::string& name) {
        return bool(tree.get_type(current_node, name));
    }

    std::shared_ptr<const Type> get_variable_type(const std::string& name) {
        return tree.get_type(current_node, name);
    }

};
}