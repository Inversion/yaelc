%skeleton "lalr1.cc"
%require "3.7"

// Write a parser header file containing definitions
// for the token kind names defined in the grammar 
// as well as a few other declarations
%defines

// Enable locations support
%locations

// parser::
%define parse.error custom

// Enable tracing
%define parse.trace

// This option with next makes 
// yylex to have signature 
// as below
%define api.token.constructor

// Declare value type - variant
// Like union, but for C++
// Can store any C++ type
%define api.value.type variant

// API namespace
%define api.namespace {mini_java}

// Token prefixes
%define api.token.prefix {MINI_JAVA_TOK_}

%code requires {
    #include <string>

    #include <ast/ast.hh>

    namespace mini_java {
        class scanner;
        class driver;
    }
//
    #include <iostream>
    using namespace std;
}

// Pass scanner as parameter to yylex
%lex-param {mini_java::scanner& scanner}

// Pass scanner as parameter to parser
%parse-param {mini_java::scanner& scanner}
// Pass driver as parameter to parser
%parse-param {mini_java::driver& driver}

%code {
    #include <scanner.hh>
    #include <driver.hh>

    static mini_java::parser::symbol_type yylex(
        mini_java::scanner& scanner
    ) {
        // this will update scanner.location
        return scanner.scanToken();
    }
}

// Declaration of tokens without values
%token
    END 0  	"end of file"
    INT		"int_t"
    FLOAT	"float_t"
    BOOL 	"bool_t"
    STRING 	"string_t"
    VOID 	"void_t"
    COMMA       ","
    ENDOFLINE 	";"
    NEWLINE 	"\n"
    ASSIGN 	"="
    STAR 	"*"
    PERCENT 	"%"
    SLASH 	"/"
    LBRACKETS   "["
    RBRACKETS   "]"
    EMPBRACKETS "[]"
    LPAREN 	"("
    RPAREN 	")"
    AND 	"&&"
    OR 		"||"
    CLASS 	"class"
    PUBLIC 	"public"
    PRIVATE 	"private"
    PROTECTED 	"protected"
    EXTENDS 	"extends"
    STATIC 	"static"
    LEFTSCOPE 	"{"
    RIGHTSCOPE 	"}"
    NEWOBJ 	"new"
    DOT 	"."
    ASSERT 	"assert"
    IF 		"if"
    ELSE 	"else"
    WHILE 	"while"
    FOR 	"for"
    NEG 	"!"
    GREATER 	">"
    LESS 	"<"
    LEQ 	"<="
    GEQ 	">="
    SUB 	"-"
    ADD 	"+"
    ISEQUAL 	"=="
    RETURN 	"return"
    ERROR
;

// Declaration of tokens with values
%token <int>            int     "int"
%token <float>          float   "float"
%token <bool>           bool    "bool"
%token <std::string>    string  "string"
%token <std::string>    id      "id"

// Declaration of nonterminals with values
%nterm <ast::AssignRep>     assign
%nterm <ast::AccessModRep>  modifier
%nterm <ast::AccessModRep>  static_modifier
%nterm <ast::ArgsRep>       args
%nterm <ast::BlockRep>      block
%nterm <ast::ClassesRep>    decl_classes
%nterm <ast::ClassRep>	    class
%nterm <ast::ConstantRep>   literal
%nterm <ast::ClassDeclRep>       class_decl
%nterm <ast::DeclsRep>           class_decls
%nterm <ast::ClassDeclMethodRep> class_decl_method
%nterm <ast::ClassDeclVarRep>    class_decl_var
%nterm <ast::StmtDeclVarRep>     stmt_decl_var
%nterm <ast::ExprRep>       expr
%nterm <ast::ExprRep>       forexpr
%nterm <ast::ExprRep>       unary_operator
%nterm <ast::ExprRep>       binary_operator
%nterm <ast::ExprsRep>      exprs
%nterm <ast::ExprRep>  field_invocation
%nterm <ast::ForRep>        for
%nterm <ast::IfStmtRep>     ifstmt
%nterm <ast::ExprRep>        lvalue
%nterm <ast::MethodCallRep> method_invocation
%nterm <ast::StmtRep>       stmt
%nterm <ast::StmtRep>       forstmt
%nterm <ast::TypeRep>       type
%nterm <ast::TypeRep>       simple_type
%nterm <ast::TypeRep>       array_type
%nterm <ast::WhileRep>      while

%nonassoc "="
%left "&&" "||"
%left "<" ">" "<=" ">=" "=="
%left "+" "-"
%left "*" "/" "%"
%left "!"
%left "." "[" "]"

%nonassoc THEN;
%nonassoc ELSE;

%%
%start program;

program:
    decl_classes[code] END
    { driver.set_result(std::move($code)); }

decl_classes:
    class decl_classes[other]
    { $$ = util::create<ast::Classes>(@$, $class, $other); }
    | %empty
    { $$ = util::create<ast::Classes>(@$); }
    | error class decl_classes[other]
    { $$ = util::create<ast::Classes>(@$, $class, $other);
    driver.update_syntax_error(@error);
    yyerrok; }

class:
    "class" id "{" class_decls "}"
    { $$ = util::create<ast::Class>(@$, $id, $class_decls); }
    | "class" id[name] "extends" id[base_name] "{" class_decls "}"
    { $$ = util::create<ast::Class>(@$, $name, $base_name, $class_decls); }

class_decls:
    class_decl class_decls[declarations]
    { $$ = util::create<ast::Decls>(@$, $class_decl, $declarations); }
    | %empty
    {$$ = util::create<ast::Decls>(@$);}

class_decl:
    class_decl_var ";"
    {$$ = $class_decl_var;}
    | class_decl_method
    {$$ = $class_decl_method;}

class_decl_var:
    type id "=" expr
    {$$ = util::create<ast::ClassDeclVar>(@$, $id, $expr, $type); }
    | type id
    {$$ = util::create<ast::ClassDeclVar>(@$, $id, util::create<ast::ExprEmpty>(@$), $type); }

class_decl_method:
    modifier[visible] static_modifier[static] type id "(" args ")" "{" block "}"
    { $$ = util::create<ast::ClassDeclMethod>(@$, $id, $type, $args, $visible, $block, mini_java::Modifiers::STATIC); }
    | modifier type id "(" args ")" "{" block "}"
    { $$ = util::create<ast::ClassDeclMethod>(@$, $id, $type, $args, $modifier, $block, mini_java::Modifiers::NOTSTATIC); }

args:
    type id "," args[arguments]
    { $$ = util::create<ast::Args>(@$, $id, $type, $arguments); }
    | type id
    { $$ = util::create<ast::Args>(@$, $id, $type); }
    | %empty
    { $$ = util::create<ast::Args>(@$); }

modifier:
    "public"
    {$$ = util::create<ast::AccessMod>(@$, mini_java::Modifiers::PUBLIC); }
    | "protected"
    {$$ = util::create<ast::AccessMod>(@$, mini_java::Modifiers::PROTECTED); }
    | "private"
    {$$ = util::create<ast::AccessMod>(@$, mini_java::Modifiers::PRIVATE); }

static_modifier:
    "static"
    {$$ = util::create<ast::AccessMod>(@$, mini_java::Modifiers::STATIC); }


type :
    simple_type {$$ = $simple_type;}
    | array_type {$$ = $array_type;}

simple_type:
    id
    { $$ = util::create<ast::SimpleType>(@$, $id); }

array_type:
    simple_type "[]"
    { $$ = util::create<ast::ArrayType>(@$, $simple_type); }

block:
    %empty 
    { $$ = util::create<ast::Block>(@$); }
    | stmt block[other]
    { $$ = util::create<ast::Block>(@$, $stmt, $other); }

stmt_decl_var:
    type id "=" expr
    { $$ = util::create<ast::StmtDeclVar>(@$, $id, $expr, $type);}
    | type id
    { $$ = util::create<ast::StmtDeclVar>(@$, $id, util::create<ast::ExprEmpty>(@$), $type);}

stmt:
    "return" expr ";"
    { $$ = util::create<ast::Return>(@$, $expr); }
    | stmt_decl_var ";"
    { $$ = $stmt_decl_var;}
    | ifstmt
    { $$ = $ifstmt; }
    | for
    { $$ = $for; }
    | while
    { $$ = $while; }
    | "{" block "}"
    { $$  = $block; }
    | expr ";"
    { $$ = util::create<ast::SimpleStmt>(@$, $expr); }

assign:
    lvalue "=" expr[value]
    { $$ = util::create<ast::Assign>(@$, $lvalue, $value); }

lvalue:
      id { $$ = util::create<ast::LvalueId>(@$, $id); }
    | id "[" expr "]" { $$ = util::create<ast::LvalueArray>(@$, $id, $expr); }
    | field_invocation { $$ = util::create<ast::LvalueField>(@$, $field_invocation); }

ifstmt:
    IF "(" expr[condition] ")" %prec THEN stmt
    { $$ = util::create<ast::IfStmt>(@$, $condition, $stmt); }
    | IF "(" expr[condition] ")" stmt[ifcode] ELSE stmt[elsecode]
    { $$ = util::create<ast::IfStmt>(@$, $condition, $ifcode, $elsecode);}

for:
    // without ';' because stmt have to end on ';'
    FOR "(" forstmt[init] ";" forexpr[condition] ";" forexpr[step] ")" stmt[code]
    { $$ = util::create<ast::For>(@$, $init, $step, $condition, $code); }
//    | FOR "(" emptyexpr[init] ";" emptyexpr[condition] ";" emptyexpr[step] ")" stmt[code]
//    { $$ = util::create<ast::For>(@$, $init, $step, $condition, $code); }

forstmt:
    forexpr[expr]
    { $$ = util::create<ast::SimpleStmt>(@$, $expr); }
    | stmt_decl_var
    { $$ = $stmt_decl_var; }

forexpr:
    %empty
    { $$ = util::create<ast::ExprEmpty>(@$); }
    | expr
    { $$ = $1; }

while:
    WHILE "(" expr[condition] ")" stmt[code]
    { $$ = util::create<ast::While>(@$, $condition, $code); }

expr:
    assign
    { $$ = $assign; }
    | binary_operator
    { $$ = $binary_operator; }
    | id "[" expr[expression] "]"
    { $$ = util::create<ast::Array>(@$, $id, $expression); }
    | "new" simple_type "[" expr[expression] "]"
    { $$ = util::create<ast::AllocateArray>(@$, $simple_type, $expression); }
    | "new" simple_type "(" ")"
    { $$ = util::create<ast::Allocate>(@$, $simple_type); }
    | unary_operator
    { $$ = $unary_operator; }
    | "(" expr[expression] ")"
    { $$ = $expression; }
    | literal[val]
    { $$ = $val; }
    | method_invocation[call]
    { $$ = $call; }
    | id
    { $$ = util::create<ast::Var>(@$, $id); }
    | field_invocation[field]
    { $$ = $field; }

method_invocation:
      id[obj] "." id[method] "(" exprs ")"
    { $$ = util::create<ast::MethodCall>(@$, $obj, $method, $exprs); }
    | id[method] "(" exprs ")"
    { $$ = util::create<ast::MethodCall>(@$, "this", $method, $exprs);}

exprs:
      %empty
    { $$ = util::create<ast::Exprs>(@$); }
    | expr
    { $$ = util::create<ast::Exprs>(@$, $expr); }
    | expr[lhs] "," exprs[rhs]
    { $$ = util::create<ast::Exprs>(@$, $lhs, $rhs); }


field_invocation:
      id[obj] "." id[field] "[" expr "]"
    { $$ = util::create<ast::FieldArrayCall>(@$, $obj, $field, $expr); }
    | id[obj] "." id[field]
    { $$ = util::create<ast::FieldCall>(@$, $obj, $field); }

literal:
      int[val]
    { $$ = util::create<ast::Constant>(@$, $val); }
    | float[val]
    { $$ = util::create<ast::Constant>(@$, $val); }
    | string[val]
    { $$ = util::create<ast::Constant>(@$, $val); }
    | bool[val]
    { $$ = util::create<ast::Constant>(@$, $val); }

binary_operator:
      expr[lhs] "&&" expr[rhs] { $$ = util::create<ast::BinOp <ast::AND> >(@$, $lhs, $rhs);}
    | expr[lhs] "||" expr[rhs] { $$ = util::create<ast::BinOp <ast::OR> >(@$, $lhs, $rhs);}
    | expr[lhs] "<" expr[rhs]  { $$ = util::create<ast::BinOp <ast::LESS> >(@$, $lhs, $rhs);}
    | expr[lhs] "<=" expr[rhs] { $$ = util::create<ast::BinOp <ast::LEQ> >(@$, $lhs, $rhs);}
    | expr[lhs] ">" expr[rhs]  { $$ = util::create<ast::BinOp <ast::GREATER> >(@$, $lhs, $rhs);}
    | expr[lhs] ">=" expr[rhs] { $$ = util::create<ast::BinOp <ast::GEQ> >(@$, $lhs, $rhs);}
    | expr[lhs] "==" expr[rhs] { $$ = util::create<ast::BinOp <ast::ISEQ> >(@$, $lhs, $rhs);}
    | expr[lhs] "+" expr[rhs]  { $$ = util::create<ast::BinOp <ast::ADD> >(@$, $lhs, $rhs);}
    | expr[lhs] "-" expr[rhs]  { $$ = util::create<ast::BinOp <ast::SUB> >(@$, $lhs, $rhs);}
    | expr[lhs] "*" expr[rhs]  { $$ = util::create<ast::BinOp <ast::MUL> >(@$, $lhs, $rhs);}
    | expr[lhs] "/" expr[rhs]  { $$ = util::create<ast::BinOp <ast::DIV> >(@$, $lhs, $rhs);}
    | expr[lhs] "%" expr[rhs]  { $$ = util::create<ast::BinOp <ast::MOD> >(@$, $lhs, $rhs);}

unary_operator:
      "-" expr { $$ = util::create<ast::UnaryOp <ast::SUB> >(@$, $expr); }
    | "!" expr { $$ = util::create<ast::UnaryOp <ast::NEG> >(@$, $expr); }
%%
