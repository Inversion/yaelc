%{
    #include <stdlib.h>

    // generated
    #include "mini_java_parser.hh"

    #include "scanner.hh"
%}

%option noyywrap nounput noinput batch debug
%option c++
%option prefix="miniJava"
%option yyclass="scanner"

id      [a-zA-Z][a-zA-Z_0-9]*
int     [0-9]+
float   [0-9]+\.[0-9]*
string  \"[^\"]*\"
blank   [ \t\r]

/* Additional states */
%x INLINE_COMMENT
%x MULTILINE_COMMENT

%{
    // Code run each time a pattern is matched.
    #define YY_USER_ACTION loc.columns(yyleng);
%}

%%
%{
    // Code run each time yylex is called.
    // To move location
    loc.step();
%}

{blank}+    loc.step();
"//".*      loc.step();
[/][*][^*]*[*]+([^*/][^*]*[*]+)*[/]       { /* comment */ loc.step(); }


"-"         return mini_java::parser::make_SUB              (loc);
"+"         return mini_java::parser::make_ADD              (loc);
"*"         return mini_java::parser::make_STAR             (loc);
"/"         return mini_java::parser::make_SLASH            (loc);
"["         return mini_java::parser::make_LBRACKETS        (loc);
"]"         return mini_java::parser::make_RBRACKETS        (loc);
"[]"        return mini_java::parser::make_EMPBRACKETS      (loc);
"("         return mini_java::parser::make_LPAREN           (loc);
")"         return mini_java::parser::make_RPAREN           (loc);
"="         return mini_java::parser::make_ASSIGN           (loc);
"%"         return mini_java::parser::make_PERCENT          (loc);
"{"         return mini_java::parser::make_LEFTSCOPE        (loc);
"}"         return mini_java::parser::make_RIGHTSCOPE       (loc);
"class"     return mini_java::parser::make_CLASS            (loc);
"public"    return mini_java::parser::make_PUBLIC           (loc);
"private"   return mini_java::parser::make_PRIVATE          (loc);
"protected" return mini_java::parser::make_PROTECTED        (loc);
"extends"   return mini_java::parser::make_EXTENDS          (loc);
"static"    return mini_java::parser::make_STATIC           (loc);
"return"    return mini_java::parser::make_RETURN           (loc);
"new"       return mini_java::parser::make_NEWOBJ           (loc);
"."         return mini_java::parser::make_DOT              (loc);
","         return mini_java::parser::make_COMMA            (loc);
";"         return mini_java::parser::make_ENDOFLINE        (loc);
"assert"    return mini_java::parser::make_ASSERT           (loc);
"if"        return mini_java::parser::make_IF               (loc);
"else"      return mini_java::parser::make_ELSE             (loc);
"while"     return mini_java::parser::make_WHILE            (loc);
"for"       return mini_java::parser::make_FOR              (loc);
"!"         return mini_java::parser::make_NEG              (loc);
">"         return mini_java::parser::make_GREATER          (loc);
"<"         return mini_java::parser::make_LESS             (loc);
"<="        return mini_java::parser::make_LEQ              (loc);
">="        return mini_java::parser::make_GEQ              (loc);
"=="        return mini_java::parser::make_ISEQUAL          (loc);
"||"        return mini_java::parser::make_OR               (loc);
"&&"        return mini_java::parser::make_AND              (loc);

{int}       return mini_java::parser::make_int              (atoi(yytext), loc);
{float}     return mini_java::parser::make_float            (atof(yytext), loc);
"true"      return mini_java::parser::make_bool             (true, loc);
"false"     return mini_java::parser::make_bool             (false, loc);
{string}    return mini_java::parser::make_string           (yytext, loc);
{id}        return mini_java::parser::make_id               (yytext, loc);

\n+         {
    // Keep track of location
    loc.lines(yyleng); loc.step();
    //return mini_java::parser::make_NEWLINE(loc);
}

<<EOF>>     return mini_java::parser::make_END        (loc);
.           return mini_java::parser::make_ERROR      (loc);
%%