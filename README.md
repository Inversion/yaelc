### YAELC

Yet Another Educational language Compiler. Compiles a miniJava language - small subset of Java.

## Compiling

`cmake -Bbuild . && cd build && make && cd ..`

## Usage

`./build/miniJavaCodeInterpreter test_input`

Prints syntax errors in form:

* position of first unexpected token
* error_ message
* part of input that was discarded

And then prints AST in xml-like format.
