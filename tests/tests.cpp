#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <string>
#include <cstdlib>

#define class struct
#define private public

#include <driver.hh>
#include <visitor/print_visitor.hh>
#include <compilation_errors.hh>
#include <visitor/type_table_visitor.hh>
#include <visitor/scope_tree_visitor.hh>
#include <frames/frame_tree_builder.hh>
#include <frames/frame_tree_walker.hh>
#include <frames/class_offsets.hh>

auto ParseAst(const std::string fname) {
    mini_java::driver driver;

    std::ifstream input(fname);
    REQUIRE(input.is_open());
    auto result = driver.parse(fname, input);
    input.close();

    auto syntax_errors = driver.get_syntax_errors();
    REQUIRE(!syntax_errors.has_error());
    REQUIRE(result);
    return *result;
}

void ComparePrintVisitor(const std::string name) {
    std::string java_code = name + ".java";
    std::string expected_out = name + ".output";
    std::ifstream output(expected_out);
    REQUIRE(output.is_open());

    std::stringstream buffer;
    buffer << output.rdbuf();
    const std::string expected = buffer.str();
    buffer = std::stringstream{};

    output.close();

    // PARSE
    auto ast = ParseAst(java_code);

    // TRAVERSE
    visitor::PrintVisitor::traverse(ast, buffer);
    const std::string actual = buffer.str();
    buffer = std::stringstream{};

    // COMPARE
    REQUIRE(expected == actual);
}

TEST_CASE("AST", "[print visitor]") {
    static const std::string test_dir = "../tests/test_inputs/";

    SECTION("Operators are parsed") {
        ComparePrintVisitor(test_dir + "bin_un_operator");
    }
    SECTION("Classes are parsed") {
        ComparePrintVisitor(test_dir + "classes");
    }
    SECTION("Fields are parsed") {
        ComparePrintVisitor(test_dir + "fields");
    }
    SECTION("Methods are parsed") {
        ComparePrintVisitor(test_dir + "methods");
    }
    SECTION("Other cases are parsed") {
        ComparePrintVisitor(test_dir + "other_cases");
    }
    SECTION("Test conditions are parsed") {
        ComparePrintVisitor(test_dir + "test_conditions");
    }
}

template <typename T>
const T* get_field(std::shared_ptr<mini_java::Type> type, const std::string& name) {
    auto type_ptr = type.get()->field(name);
    REQUIRE(type_ptr);
    auto type1 = dynamic_cast<const T*>(type_ptr.get());
    REQUIRE(type1);
    return type1;
}

const mini_java::Method* get_method(std::shared_ptr<mini_java::Type> type, const std::string& name) {
    auto type_ptr = type.get()->method(name);
    REQUIRE(type_ptr);
    auto type1 = dynamic_cast<const mini_java::Method*>(type_ptr.get());
    REQUIRE(type1);
    REQUIRE(type1->name() == name);
    return type1;
}


TEST_CASE("Type table", "[type table]") {
    static const std::string test_dir = "../tests/type_table/";
    SECTION("unknown types") {
        mini_java::CompilationErrors errors;
        auto ast = ParseAst(test_dir + "unknown.java");
        // TYPE TABLE
        auto table = visitor::TypeTableVisitor::traverse(ast, errors);
        REQUIRE(errors.has_error());
        REQUIRE(errors.count() == 3);
        auto err = errors.errors_.begin();
        REQUIRE(dynamic_cast<mini_java::UnknownType *>((err++)->get())->type_name_ == "NotExist1");
        REQUIRE(dynamic_cast<mini_java::UnknownType *>((err++)->get())->type_name_ == "NotExist2[]");
        REQUIRE(dynamic_cast<mini_java::UnknownType *>((err++)->get())->type_name_ == "NotExist3");
    }

    SECTION("redefenitions types") {
        mini_java::CompilationErrors errors;
        auto ast = ParseAst(test_dir + "redefinitions.java");
        // TYPE TABLE
        auto table = visitor::TypeTableVisitor::traverse(ast, errors);
        REQUIRE(errors.has_error());
        REQUIRE(errors.errors_.size() == 4);
        auto err = errors.errors_.begin();
        REQUIRE(dynamic_cast<mini_java::TypeRedefinition *>((err++)->get())->type_name_ == "Test1");
        REQUIRE(dynamic_cast<mini_java::VariableRedefinition *>((err++)->get())->var_name_ == "var1");
        {
            auto curr_err = dynamic_cast<mini_java::MethodRedefinition *>((err++)->get());
            REQUIRE(curr_err->class_name_ == "Test1");
            REQUIRE(curr_err->method_name_ == "method1");
        }
        {
            auto curr_err2 = dynamic_cast<mini_java::MethodArgumentRedefinition *>((err++)->get());
            REQUIRE(curr_err2->class_name_ == "Test1");
            REQUIRE(curr_err2->method_name_ == "method2");
            REQUIRE(curr_err2->arg_name_ == "x");
        }
    }

    SECTION("extends error") {
        mini_java::CompilationErrors errors;
        auto ast = ParseAst(test_dir + "extends.java");
        // TYPE TABLE
        auto table = visitor::TypeTableVisitor::traverse(ast, errors);
        REQUIRE(errors.has_error());
        REQUIRE(errors.errors_.size() == 2);
        auto err = errors.errors_.begin();
        {
            auto curr_err = dynamic_cast<mini_java::NonClassTypeExtension *>((err++)->get());
            REQUIRE(curr_err->class_name_ == "Test1");
            REQUIRE(curr_err->parent_name_ == "int");
        }
        {
            auto curr_err2 = dynamic_cast<mini_java::UnknownType *>((err++)->get());
            REQUIRE(curr_err2->type_name_ == "NonExist");
        }
    }

    SECTION("extends ok") {
        mini_java::CompilationErrors errors;
        auto ast = ParseAst(test_dir + "extends_ok.java");
        // TYPE TABLE
        auto table = visitor::TypeTableVisitor::traverse(ast, errors);
        REQUIRE(!errors.has_error());
        auto current_class = table.get("Test2");
        REQUIRE(current_class->field("a")->name() == mini_java::IntType::NAME);
        REQUIRE(current_class->field("b")->name() == mini_java::DoubleType::NAME);
        REQUIRE(current_class->field("c")->name() == mini_java::FloatType::NAME);
    }

    SECTION("Fields") {
        mini_java::CompilationErrors errors;
        auto ast = ParseAst(test_dir + "fields.java");
        // TYPE TABLE
        auto table = visitor::TypeTableVisitor::traverse(ast, errors);
        auto current_class = table.get("Test1");
        REQUIRE(!errors.has_error());

        REQUIRE(current_class->field("var1")->name() == mini_java::IntType::NAME);
        REQUIRE(current_class->field("var2")->name() == mini_java::FloatType::NAME);
        {
            auto type = get_field<mini_java::ArrayType>(current_class, "var3");
            REQUIRE(type->type.get()->name() == mini_java::StringType::NAME);
        }
        {
            auto type = get_field<mini_java::ArrayType>(current_class, "var4");
            REQUIRE(type->type.get()->name() == "Test2");
        }
        {
            auto method = get_method(current_class, "method1");
            REQUIRE(method->is_static());
            REQUIRE(method->get_access_modifier() == mini_java::Modifiers::PUBLIC);
            REQUIRE(method->return_type_.get()->name() == mini_java::VoidType::NAME);
            REQUIRE(method->args().empty());
        }
        {
            auto method = get_method(current_class, "method2");
            REQUIRE(!method->is_static());
            REQUIRE(method->get_access_modifier() == mini_java::Modifiers::PRIVATE);
            REQUIRE(method->return_type_.get()->name() == mini_java::Type::array_for(mini_java::IntType::NAME));
            REQUIRE(method->args().size() == 1);
            REQUIRE(method->args().at("a").get()->name() == mini_java::IntType::NAME);
        }
        {
            auto method = get_method(current_class, "method3");
            REQUIRE(method->is_static());
            REQUIRE(method->get_access_modifier() == mini_java::Modifiers::PROTECTED);
            REQUIRE(method->args().size() == 3);
            REQUIRE(method->return_type_.get()->name() == mini_java::IntType::NAME);
            REQUIRE(method->args().at("a").get()->name() == mini_java::Type::array_for(mini_java::IntType::NAME));
            REQUIRE(method->args().at("b").get()->name() == mini_java::DoubleType::NAME);
            REQUIRE(method->args().at("c").get()->name() == mini_java::StringType::NAME);
        }
    }
}

TEST_CASE("Scope tree", "[scope tree]") {
    static const std::string test_dir = "../tests/scopes/";
    SECTION("redefinition errors") {
        mini_java::CompilationErrors errors;
        auto ast = ParseAst(test_dir + "errors.java");
        // TYPE TABLE
        auto table = visitor::TypeTableVisitor::traverse(ast, errors);
        REQUIRE(!errors.has_error());
        // SCOPE TREE WALKER
        auto scopes = visitor::ScopeTreeVisitor::traverse(ast, table, errors);
        REQUIRE(errors.has_error());
        REQUIRE(errors.errors_.size() == 4);
        auto err = errors.errors_.begin();
        REQUIRE(dynamic_cast<mini_java::VariableRedefinition*>((err++)->get())->var_name_ == "var1");
        REQUIRE(dynamic_cast<mini_java::VariableRedefinition*>((err++)->get())->var_name_ == "a");
        REQUIRE(dynamic_cast<mini_java::VariableRedefinition*>((err++)->get())->var_name_ == "b");
        REQUIRE(dynamic_cast<mini_java::VariableRedefinition*>((err++)->get())->var_name_ == "x");
    }
    SECTION("blocks.java") {
        mini_java::CompilationErrors errors;
        auto ast = ParseAst(test_dir + "blocks.java");
        // TYPE TABLE
        auto table = visitor::TypeTableVisitor::traverse(ast, errors);
        REQUIRE(!errors.has_error());
        // SCOPE TREE WALKER
        auto scopes = visitor::ScopeTreeVisitor::traverse(ast, table, errors);
        REQUIRE(!errors.has_error());
        REQUIRE(scopes.children_size() == 1);
        scopes.begin_scope();
        {
            auto ids = scopes.get_ids();
            auto methods = scopes.get_methods();
            REQUIRE(ids.size() == 2);
            REQUIRE(methods.size() == 2);

            REQUIRE(ids.count("var1"));
            REQUIRE(ids.count("var2"));

            REQUIRE(methods.count("method1"));
            REQUIRE(methods.count("method2"));

            REQUIRE(scopes.children_size() == 2);
            // handle method1
            scopes.begin_scope();
            {
                auto ids = scopes.get_ids();
                auto methods = scopes.get_methods();
                REQUIRE(ids.size() == 2);

                REQUIRE(ids.count("var1"));
                REQUIRE(ids.count("this"));

                REQUIRE(methods.empty());
                auto var1 = scopes.get_id("var1");
                REQUIRE(var1->name() == mini_java::DoubleType::NAME);
                auto this_ = std::dynamic_pointer_cast<const mini_java::ClassType>(scopes.get_id("this"));
                REQUIRE(this_);
                REQUIRE(this_->fields().count("var1"));
                REQUIRE(this_->fields().at("var1")->name() == mini_java::IntType::NAME);
            }
            scopes.end_scope();
            // handle method2
            scopes.begin_scope();
            {
                auto ids = scopes.get_ids();
                auto methods = scopes.get_methods();
                REQUIRE(ids.size() == 3);
                REQUIRE(methods.empty());

                auto var2 = scopes.get_id("var2");
                REQUIRE(var2->name() == mini_java::IntType::NAME);

                auto x1 = scopes.get_id("x1");
                REQUIRE(x1->name() == mini_java::FloatType::NAME);

                scopes.begin_scope();
                {
                    auto ids = scopes.get_ids();
                    auto methods = scopes.get_methods();

                    REQUIRE(ids.size() == 1);
                    REQUIRE(methods.empty());
                    auto x2 = scopes.get_id("x1");
                    REQUIRE(x2->name() == mini_java::IntType::NAME);
                }
                scopes.end_scope();
            }
            scopes.end_scope();
        }
        scopes.end_scope();
    }
    SECTION("for while and if") {
        mini_java::CompilationErrors errors;
        auto ast = ParseAst(test_dir + "conditions.java");
        // TYPE TABLE
        auto table = visitor::TypeTableVisitor::traverse(ast, errors);
        REQUIRE(!errors.has_error());
        // SCOPE TREE WALKER
        auto scopes = visitor::ScopeTreeVisitor::traverse(ast, table, errors);
        REQUIRE(!errors.has_error());
        REQUIRE(scopes.children_size() == 1);
        scopes.begin_scope();
        {
            auto ids = scopes.get_ids();
            auto methods = scopes.get_methods();
            REQUIRE(ids.size() == 0);
            REQUIRE(methods.size() == 1);
            REQUIRE(scopes.children_size() == 1);

            REQUIRE(methods.count("method1"));

            // handle method1
            scopes.begin_scope();
            {
                auto ids = scopes.get_ids();
                auto methods = scopes.get_methods();
                REQUIRE(ids.size() == 3);

                REQUIRE(ids.count("x"));
                REQUIRE(ids.count("y"));
                REQUIRE(ids.count("this"));


                REQUIRE(methods.empty());

                REQUIRE(scopes.get_id("x")->name() == mini_java::FloatType::NAME);

                // handle if
                scopes.begin_scope();
                {
                    auto ids = scopes.get_ids();
                    auto methods = scopes.get_methods();

                    REQUIRE(ids.size() == 1);
                    REQUIRE(methods.empty());
                    REQUIRE(scopes.get_id("x")->name() == mini_java::IntType::NAME);
                }
                scopes.end_scope();
                // handle else
                scopes.begin_scope();
                {
                    auto ids = scopes.get_ids();
                    auto methods = scopes.get_methods();

                    REQUIRE(ids.empty());
                    REQUIRE(methods.empty());
                }
                scopes.end_scope();

                // handle for
                scopes.begin_scope();
                {
                    auto ids = scopes.get_ids();
                    auto methods = scopes.get_methods();

                    REQUIRE(ids.size() == 1);
                    REQUIRE(methods.empty());
                    REQUIRE(scopes.get_id("x")->name() == mini_java::IntType::NAME);

                    scopes.begin_scope();
                    {
                        auto ids = scopes.get_ids();
                        auto methods = scopes.get_methods();

                        REQUIRE(ids.size() == 1);
                        REQUIRE(methods.empty());
                        REQUIRE(scopes.get_id("x")->name() == mini_java::DoubleType::NAME);
                    }
                    scopes.end_scope();
                }
                scopes.end_scope();

                // handle while
                scopes.begin_scope();
                {
                    auto ids = scopes.get_ids();
                    auto methods = scopes.get_methods();

                    REQUIRE(ids.size() == 1);
                    REQUIRE(methods.empty());
                    REQUIRE(scopes.get_id("x")->name() == mini_java::IntType::NAME);
                }
                scopes.end_scope();
            }
            scopes.end_scope();
        }
        scopes.end_scope();
    }
}

TEST_CASE("Offsets & frames") {
    static const std::string test_dir = "../tests/frames/";
    SECTION("class offsets") {
        mini_java::CompilationErrors errors;
        auto ast = ParseAst(test_dir + "field_offsets.java");
        // TYPE TABLE
        auto table = visitor::TypeTableVisitor::traverse(ast, errors);
        REQUIRE(!errors.has_error());
        // SCOPE TREE WALKER
        auto scopes = visitor::ScopeTreeVisitor::traverse(ast, table, errors);
        auto class_offsets = mini_java::ClassOffsets(table);
        REQUIRE(class_offsets.offsets_.size() == 2 * (6 + 2));    // 6 simple type, 2 class

        REQUIRE(class_offsets.get_type_size(table.get("Test")) == mini_java::SimpleTypeSize::PTR);
        REQUIRE(class_offsets.get_type_size(table.get("double")) == mini_java::SimpleTypeSize::DOUBLE);

        REQUIRE(class_offsets.get_object_size("Test") ==
                              mini_java::SimpleTypeSize::PTR +
                              mini_java::SimpleTypeSize::FLOAT +
                              mini_java::SimpleTypeSize::PTR);

        REQUIRE(class_offsets.get_type_size(table.get("Empty")) == mini_java::SimpleTypeSize::PTR);
        REQUIRE(class_offsets.get_object_size("Empty") == 0);

        REQUIRE(class_offsets.get_field_offset("Test", "b") == mini_java::SimpleTypeSize::PTR + mini_java::SimpleTypeSize::FLOAT);
        REQUIRE(class_offsets.get_field_offset("Test", "c") == mini_java::SimpleTypeSize::PTR);
        REQUIRE(class_offsets.get_field_offset("Test", "d") == 0);
    }

    SECTION("Frames") {
        mini_java::CompilationErrors errors;
        auto ast = ParseAst(test_dir + "frames.java");
        // TYPE TABLE
        auto table = visitor::TypeTableVisitor::traverse(ast, errors);
        REQUIRE(!errors.has_error());
        // SCOPE TREE WALKER
        auto scopes = visitor::ScopeTreeVisitor::traverse(ast, table, errors);
        auto class_offsets = mini_java::ClassOffsets(table);
        auto frames = mini_java::FrameTreeBuilder::get_offsets(scopes, class_offsets);

        REQUIRE(frames.current_scope->children.size() == 1);

        frames.begin_scope();
        REQUIRE(frames.current_scope->children.size() == 1);
        frames.begin_scope();
        REQUIRE(frames.current_scope->children.size() == 2);
        REQUIRE(frames.get_variable_offset("this") == 8);
        REQUIRE(frames.get_variable_offset("x2") == 4);
        REQUIRE(frames.get_variable_offset("x3") == 0);
        REQUIRE(frames.current_scope->offsets.size() == 3);

        frames.begin_scope();
        REQUIRE(frames.get_variable_offset("x4") == 12);

        frames.end_scope();
        frames.begin_scope();
        REQUIRE(frames.get_variable_offset("x5") == 12);

        frames.end_scope();
        frames.end_scope();
        frames.end_scope();
    }
}