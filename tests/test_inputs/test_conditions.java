class TestIf {
    public static void main() {
        int a;
        int b;
        int x;
        int c;

        if (a > b) {
            int c = 0;
        } else if (a - 1 > b) {
            int c = 1;
        } else if (a - 2 > b) {
            int c = 2;
        } else {
            c = 3;
        }
        if (a > b) x = 1; else if (a < b) x = 2; else x = 3;

        if (a > b) {
            int d;
            if (d > b) {
                int cc = 0;
                x = x + cc;
            } else
                x = x - d;
        }
    }
}
//test comment

class TestLoops {
    public static void main() {
        for (int i = 0; i < 100; i = i + 1) {
            // comment line test
            int c = 0;
            for (c = 12; c < 123; c = c + i)
                c = c - 1;
        }
        while (0) x = x + 1;
        while (0) {}

        int a = 0;
        int b;

        while (a > b)
            while (a > b)
                for (;;) a = a + 1;
    }
}