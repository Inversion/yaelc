class TestMethods1 {
    public static void test1(int x) {
        {{{{{int x = (123 + 1) + 2;}}}}}
    }
    private static int test2(int x) {
        int[] z;
        test1(x, "asd", z);
        return z;
    }
    protected static int test3() {
        int x = 1+(1+(1));
        return test2((((((x + 123))))));
    }
    public int[] test4() {{{
        test3();
        return new int[123+2];
    }}}
}

class TestMethods2 {
    public void main() {
        int[] z = obj.test4();
        obj.test4();
        obj.test3();
        obj.test1(1, "asd", z);
        int x = 32 + obj.test2(123+52);
        int y = (obj.test3());

        TestMethods1 obj2;
        int[] z = obj2.test4();
        obj2.test4();
        obj2.test3();
        obj2.test1(1, "asd", z);
        int x = 32 + obj2.test2(123+52);
        int y = (obj2.test3());
    }

    TestMethods1 obj;
}